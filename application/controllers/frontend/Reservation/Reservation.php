<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reservation extends CI_Controller {

	public function __construct(){
    parent::__construct();

		if(empty($this->session->userdata('userdata'))){
			$this->session->sess_destroy();
			redirect(base_url("login"));
		}

		$this->dCurrent = date('Y-m-d H:i:s');
    $this->load->model('Home_model');

  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$aUserData = $this->session->userdata('userdata');
		// if($aUserData['is_manager'] == 1){
		// 	$this->data["aData"] = $this->Home_model->getIssueMyDept($aUserData['department_name']);
		// }else{
		// 	$this->data["aData"] = $this->Home_model->getMyIssue($aUserData['user_id']);
		// }
		$this->load->model("Member_model");
		$this->data["aTime"]	=	$this->Member_model->getTime();
		$this->data["aUserData"]	=	$aUserData;
		$this->load->view('frontend/reservation/index',$this->data);
	}

	public function setFormatDate($value)
	{
		$arr = explode("/",$value);
		$newDate = $arr[1]."/".$arr[0]."/".$arr[2];
		return $newDate;
	}

	public function step2()
	{
		// echo "<pre>";
		// print_r($this->session->userdata());
		// die();
		$this->load->model("Member_model");
		$aPost = $this->input->post();
		$Reserv_id = $this->input->get("reserv_id");
		$userdata	=	$this->session->userdata("userdata");
		$this->data["aData"]	=	$this->Member_model->getUserMyDept($userdata["id"]);

		if(!empty($_POST)){
			$aSession = array(
				'Reserv_id' => $Reserv_id,
				'dReserv' => date('Y-m-d',strtotime($this->setFormatDate($this->input->post("dReserv")))),
				'tReserv' => $aPost["tReserv"],
			);
			// echo "1";
		}else{
			$aDataReserv = $this->session->userdata($Reserv_id);

			$member_id 			= $userdata["id"];
			$reserv_date 		= $aDataReserv["dReserv"];
			$time_id 				= $aDataReserv["tReserv"];

			$aSession = array(
				'Reserv_id' => $Reserv_id,
				'dReserv' => $reserv_date,
				'tReserv' => $time_id,
			);
			// echo "2";
		}

		$this->session->set_userdata($Reserv_id,$aSession);

		$aUserData = $this->session->userdata($Reserv_id);
		// if($aUserData['is_manager'] == 1){
		// 	$this->data["aData"] = $this->Home_model->getIssueMyDept($aUserData['department_name']);
		// }else{
		// 	$this->data["aData"] = $this->Home_model->getMyIssue($aUserData['user_id']);
		// }
		$this->data["Reserv_id"]	=	$aUserData;
		$this->data["aUserData"]	=	$this->session->userdata("userdata");
		$this->load->view('frontend/reservation/step2',$this->data);
	}

	public function step3()
	{
		$aBus = $this->input->post("flat-checkbox-1");
		$aFood = $this->input->post("flat-checkbox-2");
		if(empty($aBus) && empty($aFood)){
			$this->session->set_flashdata('msg-danger','ผิดพลาด กรุณาเลือกอย่างน้อย 1 รายการ เพื่อจองรถ หรือ ต้องการอาหาร');
			redirect($this->agent->referrer(),'refresh');
		}
		$this->load->model("Member_model");
		$userdata	=	$this->session->userdata("userdata");
		$this->data["aData"]	=	$this->Member_model->getsupmember($userdata["id"]);

		$Reserv_id = $this->input->get("reserv_id");
		$aDataReserv = $this->session->userdata($Reserv_id);

		$member_id 			= $userdata["id"];
		$reserv_date 		= $aDataReserv["dReserv"];
		$time_id 				= $aDataReserv["tReserv"];

		$aDataUpdate = array('deleted' => 1, );
		$this->Member_model->setDeleted_1($member_id,$reserv_date,$time_id,$aDataUpdate);

		foreach ($this->data["aData"] as $key => $vData) {

			$dCurrent = date('Y-m-d H:i:s');

			$aInsert = array(
				'emp_no' 					=> $vData["emp_no"],
				'reserv_no' 			=> $Reserv_id,
				'member_id' 			=> $member_id,
				'reserv_date' 		=> $reserv_date,
				'time_id' 				=> $time_id,
				'checkpoint' 			=> 1,
				'bus' 						=> 0,
				'food' 						=> 0,
				'createdate' 			=> $dCurrent,
				'updatedate' 			=> $dCurrent,
				'deleted' 				=> 0,
			);

			if(!empty($aBus)){
				if (in_array($vData["emp_no"], $aBus))
			  {
			  	$aInsert['bus'] = 1;
			  }
			}

			if(!empty($aFood)){
				if (in_array($vData["emp_no"], $aFood))
			  {
			  	$aInsert['food'] = 1;
			  }
		  }

			if($aInsert['bus'] == 1 || $aInsert['food'] == 1){
				$res = $this->Member_model->saveReservaion($aInsert);
			}

		}
		$this->load->model("Member_model");
		$Reserv_id = $this->input->get("reserv_id");
		$userdata	=	$this->session->userdata("userdata");
		$aUserData = $this->session->userdata($Reserv_id);

		$this->data["Reserv_id"]	=	$aUserData;
		$this->data["aUserData"]	=	$this->session->userdata("userdata");

		$this->load->view('frontend/reservation/step3',$this->data);

	}

	public function cancel_reserv()
	{
		$id = $this->input->post("id");
		$aData = array(
			'updatedate' => date("Y-m-d H:i:s"),
			'deleted' => 1,
		);
		$this->db->where('id',$id);


		echo json_encode($this->db->update('reservation_tbl',$aData));
	}

	public function view()
	{
		$issue_id = $this->input->get('issue_id');
		$this->data['aData']	=	$this->Home_model->getIssueByID($issue_id);
		// echo "<pre>";
		// print_r($this->data['aData']);
		// die();
		$this->load->view('frontend/helpdesk/view',$this->data);
	}
	public function issue()
	{
		$this->load->view('frontend/helpdesk/form');
	}

	public function ApproveManager()
	{
		$status_change = 1;
		$aUserData = $this->session->userdata('userdata');
		$issue_id = $this->input->get('issue_id');
		$aData = array(
			'approve_manager_by' => $aUserData['firstname']." ".$aUserData['lastname'],
			'approve_manager_datetime' => date('Y-m-d H:i:s'),
			'status' => 0,
			'updatedate' => date('Y-m-d H:i:s'),
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);

		$this->load->model('Home_model');
		$rData	=	$this->Home_model->getEmailByIssueID($issue_id);
		$aEmail = array($rData['user_email']);

		$this->send_mail($issue_id,$aEmail,$status_change);
		// redirect(base_url('helpdesk/home'),'refresh');
	}

	public function rejectManager()
	{
		$status_change = 1;
		$aUserData = $this->session->userdata('userdata');
		$issue_id = $this->input->get('issue_id');
		$aData = array(
			'approve_manager_by' => $aUserData['firstname']." ".$aUserData['lastname'],
			'approve_manager_datetime' => date('Y-m-d H:i:s'),
			'status' => 2, // 2 = cancel
			'updatedate' => date('Y-m-d H:i:s'),
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);

		$this->load->model('Home_model');
		$rData	=	$this->Home_model->getEmailByIssueID($issue_id);
		$aEmail = array($rData['user_email']);

		$this->send_mail($issue_id,$aEmail,$status_change);
		// redirect(base_url('helpdesk/home'),'refresh');
	}

	public function save()
	{
		$status_change = 0;
		$aUserData = $this->session->userdata('userdata');
		$aData = array(
			'name_of_req' => $this->input->post('sFullName'),
			'user_id' => $aUserData['user_id'],
			'com_name' => $this->input->post('sCompName'),
			'dept_name' => $this->input->post('iDept'),
			'phone_no' => $this->input->post('sTel'),
			'type_of_req' => serialize($this->input->post('inline-checkbox')),
			'case_of_problem' => $this->input->post('sDetail'),
			'status' => 0,
			'createdate' => $this->dCurrent,
			'updatedate' => $this->dCurrent,
			'deleted' => 0,
		);
		$this->db->insert('issue_tbl',$aData);
		$issueNewID = $this->db->insert_id();
		$this->load->model('Home_model');
		$aEmailData = $this->Home_model->getEmailManagerByDepID($aUserData['department_id']);
		$aEmail = array();
		foreach ($aEmailData as $kEmailData => $vEmailData) {
			array_push($aEmail,$vEmailData['user_email']);
		}
		$this->send_mail($issueNewID,$aEmail,$status_change);
		// redirect(base_url("helpdesk/home"),'refresh');
	}

	public function send_mail($issue_id,$aEmail,$status_change) {

		$aEmailTest = array();
		array_push($aEmailTest,'sornram_sukpi@hlt.co.th');
		$this->load->library('MY_Email');
		$this->email->from('sornram_sukpi@hlt.co.th');
		$this->email->to(
					// $aEmail
					$aEmailTest
			);
	  $this->email->cc(array('sornram_sukpi@hlt.co.th'));
		$this->load->model('Home_model');
		$aData	=	$this->Home_model->getIssueByID($issue_id);
		$aSerial = unserialize($aData['type_of_req']);
		$htmlContent = '<br>';
		$htmlContent .= '<center>
		<h3>
			<strong class="text-danger pull-left" style="color:red;">Honda Lock</strong>
			<strong>แบบบันทึกการแจ้งซ่อม Hardware, Software และ Network</strong>
			<span class="pull-right"><strong>Issue No : </strong><u>'.sprintf("%05d",$aData["issue_id"]).'</u></span>
		</h3>
		<hr>
<center>
		<table width="85%">
			<tbody>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Name of Requster :</b></td>
					<td>'.$aData["name_of_req"].'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Computer name :</b></td>
					<td>'.$aData["com_name"].'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Department :</b></td>
					<td>'.$aData["dept_name"].'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Phone No. :</b></td>
					<td>'.$aData["phone_no"].'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Type of request :</b></td>
					<td>'.json_encode($aSerial).'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Case of Problem :</b></td>
					<td>'.$aData["case_of_problem"].'</td>
				</tr>';

				if($status_change == 1){
					if(!empty($aData["approve_manager_by"]) && $aData["status"] == 0){
						$textColor = 'green';
						$statusByManager = 'Approve';
					}else if($aData["status"] == 2){
						$textColor = 'red';
						$statusByManager = 'Reject';
					}
					$htmlContent .= '
					<tr>
						<td width="33%"></td>
						<td width="15%" height="30px"><b>Status :</b></td>
						<td style="color:'.$textColor.'">'.$statusByManager.'</td>
					</tr>
					';
				}

				$htmlContent .= '
			</tbody>
		</table>
</center>
		<hr>';
		if($status_change == 0){
			$htmlContent .= '<p><a href="'.base_url("helpdesk/login").'" target="_blank">'.base_url("helpdesk/login").'</a></p></center>';
		}
			$this->email->subject("Test Issue No. ".sprintf("%05d",$aData["issue_id"])."");
			$this->email->message($htmlContent);
		//
			if(!empty($aEmail)){
				if($this->email->send()){
					// echo 'OK';
				}else{
					$this->session->set_flashdata('msg-danger','Error. Email not sent, Please contact to administrator.');
				}
			}
	}

}
