<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct(){
    parent::__construct();
		$this->dCurrent = date('Y-m-d H:i:s');
    $this->load->model('Register_model');

  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->data["aDept"] = $this->Register_model->getDept();
		$this->load->view('frontend/register/form',$this->data);
	}

	public function save()
	{
		$aData = array(
			'username' 			=> htmlspecialchars($this->input->post('sUsername')),
			'password' 			=> md5($this->input->post('sPassword')),
			'firstname' 		=> htmlspecialchars($this->input->post('sFirstname')),
			'lastname' 			=> htmlspecialchars($this->input->post('sLastname')),
			'computer_name' => htmlspecialchars($this->input->post('sCompName')),
			'department_id' => htmlspecialchars($this->input->post('iDept')),
			'tel' 					=> htmlspecialchars($this->input->post('sTel')),
			'user_email' 		=> $this->input->post('user_email'),
			'createdate'		=>	$this->dCurrent,
			'updatedate'		=>	$this->dCurrent,
			'deleted'				=>	0,
		);
		$this->load->model('Register_model');
		$aUsername = $this->Register_model->chkDupUsername(htmlspecialchars($this->input->post('sUsername')));
		if(empty($aUsername)){
			$this->Register_model->save($aData);
			$this->session->set_flashdata('msg-success','Success, Thank you.');
		}else{
			$this->session->set_flashdata('msg-danger','Error, Please try again');
		}
		redirect(base_url("helpdesk/register"),'refresh');
	}
}
