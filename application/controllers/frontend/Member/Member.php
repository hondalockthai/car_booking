<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct(){
    parent::__construct();

		if(empty($this->session->userdata('userdata'))){
			$this->session->sess_destroy();
			redirect(base_url("login"));
		}

		$this->dCurrent = date('Y-m-d H:i:s');
    $this->load->model('Home_model');

  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// if($aUserData['is_manager'] == 1){
		// 	$this->data["aData"] = $this->Home_model->getIssueMyDept($aUserData['department_name']);
		// }else{
		// 	$this->data["aData"] = $this->Home_model->getMyIssue($aUserData['user_id']);
		// }
		$this->load->model("Member_model");
		$userdata	=	$this->session->userdata("userdata");
		$this->data["aData"]	=	$this->Member_model->getUserMyDept($userdata["id"]);
		$this->data["aTime"]	=	$this->Member_model->getTime();
		$this->data["aUserData"]	=	$userdata;
		$this->load->view('frontend/member/index',$this->data);
	}

	public function emp_buspoint()
	{
		$this->load->model("Member_model");

		$userdata	=	$this->session->userdata("userdata");

		$this->data["aUserData"]	=	$userdata;
		$this->data["bus"]	=	$this->Member_model->getBus_All();
		$this->load->view('frontend/member/edit',$this->data);
	}

}
