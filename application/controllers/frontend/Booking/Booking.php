<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Booking extends CI_Controller {

	public function __construct(){
    parent::__construct();

		if(empty($this->session->userdata('userdata'))){
			$this->session->sess_destroy();
			redirect(base_url("login"));
		}

		$this->dCurrent = date('Y-m-d H:i:s');
    $this->load->model('Home_model');

  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// if($aUserData['is_manager'] == 1){
		// 	$this->data["aData"] = $this->Home_model->getIssueMyDept($aUserData['department_name']);
		// }else{
		// 	$this->data["aData"] = $this->Home_model->getMyIssue($aUserData['user_id']);
		// }
		$this->load->model("Member_model");
		$userdata	=	$this->session->userdata("userdata");
		$this->data["aData"]	=	$this->Member_model->getsupmember($userdata["id"]);
		$this->data["aTime"]	=	$this->Member_model->getTime();
		$this->data["aUserData"]	=	$userdata;
		$this->load->view('frontend/booking/index2',$this->data);
	}

	public function jsonviewdata()
	{
		$aJson = array ("aaData" => array ());
		if($this->uri->segment(3) != '' && $this->uri->segment(4) != '' && $this->uri->segment(5) != ''){
			$dDate = $this->input->post();
			// echo "<pre>";
			// print_r($dDate);
			// die();
			$sDate = date("Y-m-d",strtotime($this->uri->segment(5)."-".$this->uri->segment(4)."-".$this->uri->segment(3)));
			$iNum = 1;
	    $iCountItem = 0;

			$this->db->select('*,reservation_tbl.id as reservID');
			$this->db->from('reservation_tbl');
			$this->db->join('user_tbl','user_tbl.emp_no = reservation_tbl.emp_no','left');
			$this->db->join('buspoint_tbl','buspoint_tbl.id = user_tbl.buspoint_id','left');
			$this->db->join('bus_tbl','bus_tbl.id = buspoint_tbl.idbus','left');
			$this->db->where('reserv_date',$sDate);
			$this->db->where('reservation_tbl.deleted',0);
			$qry = $this->db->get();
			$aData = $qry->result_array();

			$cCountData = count($aData);
			// $cCountData = 0;

			if ($cCountData >= 1) {

					foreach ($aData as $kData => $vData) {

							$aJson ['aaData'] [$kData] [] = $iNum;
							$aJson ['aaData'] [$kData] [] = $vData["emp_no"];
							$aJson ['aaData'] [$kData] [] = $vData["prefix_name"].$vData["user_fname"]." ".$vData["user_lname"];;
							if($vData["bus"] == 1){
								$aJson ['aaData'] [$kData] [] = '<i class="glyphicon glyphicon-ok"></i>';
							}else{
								$aJson ['aaData'] [$kData] [] = '<i class="glyphicon glyphicon-minus"></i>';
							}
							if($vData["food"] == 1){
								$aJson ['aaData'] [$kData] [] = '<i class="glyphicon glyphicon-ok"></i>';
							}else{
								$aJson ['aaData'] [$kData] [] = '<i class="glyphicon glyphicon-minus"></i>';
							}
							$aJson ['aaData'] [$kData] [] = $vData["bus_name"];
							$aJson ['aaData'] [$kData] [] = '<a class="btn btn-danger" onclick="myFunction('.$vData["reservID"].')"><i class="fa fa-remove"></i> ยกเลิก</a>';
							// $aJson ['aaData'] [$iCountItem] [] = $vData["company_name"];
							// $aJson ['aaData'] [$iCountItem] [] = $vData["fname"]." ".$vData["lname"];
							// $aJson ['aaData'] [$iCountItem] [] = $vData["phone"];
							// $aJson ['aaData'] [$iCountItem] [] = $vData["id_contact"];
							// $aJson ['aaData'] [$iCountItem] [] = $aPlantData;
							// $aJson ['aaData'] [$iCountItem] [] = '
							// <div class="btn-group">
							// 		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							// 		<i class="text-primary fa fa-cogs" aria-hidden="true"></i> <span class="caret"></span>
							// 		</button>
							// 		<ul class="dropdown-menu">
							// 				<li><a href='.base_url("manage_contact_temp/formData/".$vData["contact_temp_id"]).'><i class="fa fa-pencil-square-o" aria-hidden="true"></i> แก้ไข</a></li>
							// 				<li><a id="btnDelete" href='.base_url("manage_contact_temp/deleteData/".$vData["contact_temp_id"]).'><i class="fa fa-trash-o" aria-hidden="true"></i> ลบ</a></li>
							// 		</ul>
							// </div>
							// ';

							$iCountItem++;
							$iNum++;
					}


			}
		}else{

		}
		echo json_encode($aJson);
	}

	public function updateuser()
	{
		$this->load->library('excel');
		$file = 'C:\xampp\htdocs\car_booking\assets\emp.xlsx';
		$inputFileName = $file;
		$aData = array();
		try {

	        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	        $objPHPExcel = $objReader->load($inputFileName);
	        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
					$cnt = 1;
					foreach ($allDataInSheet as $key => $value) {
						if($key == 1){
							$cnt++;
							continue;
						}
						if(substr($value['B'], 0, 1) == '1'){
							continue;
						}
						$pwd = strtoupper(substr($value['G'], 0, 1)).strtolower(substr($value['G'], 1, 1)).strtoupper(substr($value['H'], 0, 1)).strtolower(substr($value['H'], 1, 1))."@".substr($value['L'], 9, 4);

						$DC 		= array("ZN Die Casting" , "Mg Die Casting" , "Die Casting");
						$PO 		= array("Plastic  Operation" , "Plastic Operation Office");
						$PA 		= array("Painting" , "Painting Office");
						$Assy1 	= array(
															"Assembly1 Door Mirror Main Line" , "Assembly1 Door Mirror Sub Line-act" ,"Assembly1 Door Mirror Sub Line-bkt",
															"Assembly1 Out Handle Main Line","Assembly1 Out Handle Sub Line","Assembly-1",
													);
						$Assy2 	= array("Assembly2 2R Key set" , "Assembly2 Wheel sensor","Assembly2 4R Key Set","Assembly-2","Assembly2 Electric Steering Lock");
						$PDC 		= array("Production Control");
						$PRO 		= array("Procurement");
						$SE 		= array("Safety&Environmental");
						$ME 		= array("Manufacturing Engineering");
						$DM 		= array("Die Making");
						$PQ 		= array("Parts  Quality");
						$HR 		= array("Human Resource & General Affairs","General Affairs");
						$ACC 		= array("Accounting");
						$NMP 		= array("New Model Promotion");
						$CS 		= array("Cost&Sourcing");
						$ISO 		= array("ISO Center");
						$TQM 		= array("TQM Control & Interpreter");
						$MPQ 		= array("Mass Production Quality");
						$NMQ 		= array("New Model Quality");
						$SAL 		= array("Sale");
						$PUR 		= array("Purchasing");
						$SA 		= array("Sales Asia");
						$QS 		= array("Quality  System");

						if(in_array($value['K'],$DC)){
							$dept_id	= 10;
						}else if(in_array($value['K'],$PO)){
							$dept_id	=	9;
						}else if(in_array($value['K'],$PA)){
							$dept_id	=	13;
						}else if(in_array($value['K'],$Assy1)){
							$dept_id	= 15;
						}else if(in_array($value['K'],$Assy2)){
							$dept_id	=	16;
						}else if(in_array($value['K'],$PDC)){
							$dept_id	=	5;
						}else if(in_array($value['K'],$PRO)){
							$dept_id	=	22;
						}else if(in_array($value['K'],$SE)){
							$dept_id	=	2;
						}else if(in_array($value['K'],$ME)){
							$dept_id	=	21;
						}else if(in_array($value['K'],$DM)){
							$dept_id	=	17;
						}else if(in_array($value['K'],$PQ)){
							$dept_id	=	20;
						}else if(in_array($value['K'],$HR)){
							$dept_id	=	19;
						}else if(in_array($value['K'],$ACC)){
							$dept_id	=	3;
						}else if(in_array($value['K'],$NMP)){
							$dept_id	=	6;
						}else if(in_array($value['K'],$CS)){
							$dept_id	=	7;
						}else if(in_array($value['K'],$ISO)){
							$dept_id	= 28;
						}else if(in_array($value['K'],$TQM)){
							$dept_id	= 4;
						}else if(in_array($value['K'],$MPQ)){
							$dept_id	=	23;
						}else if(in_array($value['K'],$NMQ)){
							$dept_id	= 30;
						}else if(in_array($value['K'],$SAL)){
							$dept_id	=	26;
						}else if(in_array($value['K'],$PUR)){
							$dept_id	= 32;
						}else if(in_array($value['K'],$SA)){
							$dept_id	= 31;
						}else if(in_array($value['K'],$QS)){
							$dept_id	= 33;
						}else{
							$dept_id	= NULL;
						}

						$arrayName = array(
							'emp_no' => $value['B'],
							'prefix_name' => $value['C'],
							'user_fname' => $value['D'],
							'user_lname' => $value['E'],
							'dept_name' => $value['K'],
							'dept_id' => $dept_id,
							'buspoint_id' => NULL,
							'user_password' => $pwd,
							'createdate' => date("Y-m-d H:i:s"),
							'updatedate' => date("Y-m-d H:i:s"),
							'deleted' => 0,
						);
						array_push($aData,$arrayName);
					}
				} catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                    . '": ' .$e->getMessage());
        }
				$this->db->insert_batch('user_tbl',$aData);
	}

	public function updatebuspoint()
	{
		$this->load->library('excel');
		$file = 'C:\xampp\htdocs\car_booking\assets\emp_bus.xlsx';
		$inputFileName = $file;
		$aData = array();
		try {

	        $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
	        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	        $objPHPExcel = $objReader->load($inputFileName);
	        $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
					$cnt = 1;
					foreach ($allDataInSheet as $key => $value) {
						$buspoint = $value['H'];
						$arrayName = array(
							'buspoint_id' => $buspoint,
							'updatedate' => date("Y-m-d H:i:s"),
						);

						$this->db->where('emp_no',$value['C']);
						$this->db->update('user_tbl',$arrayName);
						// $this->db->update('submember_tbl',$arrayName);
					}
				} catch (Exception $e) {
            die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                    . '": ' .$e->getMessage());
        }
	}

	public function setFormatDate($value)
	{
		$arr = explode("/",$value);
		$newDate = $arr[1]."/".$arr[0]."/".$arr[2];
		return $newDate;
	}

	public function step2()
	{

		$this->load->model("Member_model");
		$aPost = $this->input->post();
		$Reserv_id = $this->input->get("reserv_id");
		$userdata	=	$this->session->userdata("userdata");
		$this->data["aData"]	=	$this->Member_model->getsupmember($userdata["id"]);

		$aSession = array(
			'Reserv_id' => $Reserv_id,
			'dReserv' => date('Y-m-d',strtotime($this->setFormatDate($this->input->post("dReserv")))),
			'tReserv' => $aPost["tReserv"],
		);

		$this->session->set_userdata($Reserv_id,$aSession);

		$aUserData = $this->session->userdata($Reserv_id);
		// if($aUserData['is_manager'] == 1){
		// 	$this->data["aData"] = $this->Home_model->getIssueMyDept($aUserData['department_name']);
		// }else{
		// 	$this->data["aData"] = $this->Home_model->getMyIssue($aUserData['user_id']);
		// }
		$this->data["Reserv_id"]	=	$aUserData;
		$this->data["aUserData"]	=	$this->session->userdata("userdata");
		$this->load->view('frontend/reservation/step2',$this->data);
	}

	public function step3()
	{
			$this->load->model("Member_model");
			$Reserv_id = $this->input->get("reserv_id");
			$userdata	=	$this->session->userdata("userdata");
			$aUserData = $this->session->userdata($Reserv_id);

			$this->data["Reserv_id"]	=	$aUserData;
			$this->data["aUserData"]	=	$this->session->userdata("userdata");

			$this->load->view('frontend/reservation/step3',$this->data);
	}

	public function view()
	{
		$issue_id = $this->input->get('issue_id');
		$this->data['aData']	=	$this->Home_model->getIssueByID($issue_id);
		// echo "<pre>";
		// print_r($this->data['aData']);
		// die();
		$this->load->view('frontend/helpdesk/view',$this->data);
	}
	public function issue()
	{
		$this->load->view('frontend/helpdesk/form');
	}

	public function ApproveManager()
	{
		$status_change = 1;
		$aUserData = $this->session->userdata('userdata');
		$issue_id = $this->input->get('issue_id');
		$aData = array(
			'approve_manager_by' => $aUserData['firstname']." ".$aUserData['lastname'],
			'approve_manager_datetime' => date('Y-m-d H:i:s'),
			'status' => 0,
			'updatedate' => date('Y-m-d H:i:s'),
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);

		$this->load->model('Home_model');
		$rData	=	$this->Home_model->getEmailByIssueID($issue_id);
		$aEmail = array($rData['user_email']);

		$this->send_mail($issue_id,$aEmail,$status_change);
		// redirect(base_url('helpdesk/home'),'refresh');
	}

	public function rejectManager()
	{
		$status_change = 1;
		$aUserData = $this->session->userdata('userdata');
		$issue_id = $this->input->get('issue_id');
		$aData = array(
			'approve_manager_by' => $aUserData['firstname']." ".$aUserData['lastname'],
			'approve_manager_datetime' => date('Y-m-d H:i:s'),
			'status' => 2, // 2 = cancel
			'updatedate' => date('Y-m-d H:i:s'),
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);

		$this->load->model('Home_model');
		$rData	=	$this->Home_model->getEmailByIssueID($issue_id);
		$aEmail = array($rData['user_email']);

		$this->send_mail($issue_id,$aEmail,$status_change);
		// redirect(base_url('helpdesk/home'),'refresh');
	}

	public function save()
	{
		$status_change = 0;
		$aUserData = $this->session->userdata('userdata');
		$aData = array(
			'name_of_req' => $this->input->post('sFullName'),
			'user_id' => $aUserData['user_id'],
			'com_name' => $this->input->post('sCompName'),
			'dept_name' => $this->input->post('iDept'),
			'phone_no' => $this->input->post('sTel'),
			'type_of_req' => serialize($this->input->post('inline-checkbox')),
			'case_of_problem' => $this->input->post('sDetail'),
			'status' => 0,
			'createdate' => $this->dCurrent,
			'updatedate' => $this->dCurrent,
			'deleted' => 0,
		);
		$this->db->insert('issue_tbl',$aData);
		$issueNewID = $this->db->insert_id();
		$this->load->model('Home_model');
		$aEmailData = $this->Home_model->getEmailManagerByDepID($aUserData['department_id']);
		$aEmail = array();
		foreach ($aEmailData as $kEmailData => $vEmailData) {
			array_push($aEmail,$vEmailData['user_email']);
		}
		$this->send_mail($issueNewID,$aEmail,$status_change);
		// redirect(base_url("helpdesk/home"),'refresh');
	}

	public function send_mail($issue_id,$aEmail,$status_change) {

		$aEmailTest = array();
		array_push($aEmailTest,'sornram_sukpi@hlt.co.th');
		$this->load->library('MY_Email');
		$this->email->from('sornram_sukpi@hlt.co.th');
		$this->email->to(
					// $aEmail
					$aEmailTest
			);
	  $this->email->cc(array('sornram_sukpi@hlt.co.th'));
		$this->load->model('Home_model');
		$aData	=	$this->Home_model->getIssueByID($issue_id);
		$aSerial = unserialize($aData['type_of_req']);
		$htmlContent = '<br>';
		$htmlContent .= '<center>
		<h3>
			<strong class="text-danger pull-left" style="color:red;">Honda Lock</strong>
			<strong>แบบบันทึกการแจ้งซ่อม Hardware, Software และ Network</strong>
			<span class="pull-right"><strong>Issue No : </strong><u>'.sprintf("%05d",$aData["issue_id"]).'</u></span>
		</h3>
		<hr>
<center>
		<table width="85%">
			<tbody>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Name of Requster :</b></td>
					<td>'.$aData["name_of_req"].'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Computer name :</b></td>
					<td>'.$aData["com_name"].'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Department :</b></td>
					<td>'.$aData["dept_name"].'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Phone No. :</b></td>
					<td>'.$aData["phone_no"].'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Type of request :</b></td>
					<td>'.json_encode($aSerial).'</td>
				</tr>
				<tr>
					<td width="33%"></td>
					<td width="15%" height="30px"><b>Case of Problem :</b></td>
					<td>'.$aData["case_of_problem"].'</td>
				</tr>';

				if($status_change == 1){
					if(!empty($aData["approve_manager_by"]) && $aData["status"] == 0){
						$textColor = 'green';
						$statusByManager = 'Approve';
					}else if($aData["status"] == 2){
						$textColor = 'red';
						$statusByManager = 'Reject';
					}
					$htmlContent .= '
					<tr>
						<td width="33%"></td>
						<td width="15%" height="30px"><b>Status :</b></td>
						<td style="color:'.$textColor.'">'.$statusByManager.'</td>
					</tr>
					';
				}

				$htmlContent .= '
			</tbody>
		</table>
</center>
		<hr>';
		if($status_change == 0){
			$htmlContent .= '<p><a href="'.base_url("helpdesk/login").'" target="_blank">'.base_url("helpdesk/login").'</a></p></center>';
		}
			$this->email->subject("Test Issue No. ".sprintf("%05d",$aData["issue_id"])."");
			$this->email->message($htmlContent);
		//
			if(!empty($aEmail)){
				if($this->email->send()){
					// echo 'OK';
				}else{
					$this->session->set_flashdata('msg-danger','Error. Email not sent, Please contact to administrator.');
				}
			}
	}

}
