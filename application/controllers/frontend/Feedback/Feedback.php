<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {

	public function __construct(){
    parent::__construct();

		if(empty($this->session->userdata('userdata'))){
			$this->session->sess_destroy();
			redirect(base_url("helpdesk/login"));
		}

		$this->dCurrent = date('Y-m-d H:i:s');
    $this->load->model('Home_model');

  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function update()
	{
		$issue_id	= $this->input->post('issue_id');
		$aFeedback = array(
	    'feedback_level1'	=> $this->input->post('feedback_level1'),
	    'feedback_level2'	=> $this->input->post('feedback_level2'),
	    'feedback_level3'	=> $this->input->post('feedback_level3'),
		);
		$serailData  = serialize($aFeedback);
		$aData = array(
			'feedback' => $serailData,
			'updatedate' => $this->dCurrent,
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);
		$this->session->set_flashdata('msg-success','Thank you for feedback. ;D');
		redirect(base_url("helpdesk/home"),'refresh');
	}

}
