<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {

        parent::__construct();
        // Your own constructor code
    }

    /*
    * Authentication User
    */
    public function auth_user_login() {
		$user = $this->security->xss_clean($this->input->post('sUserName'));
		$pass = $this->security->xss_clean($this->input->post('sPassword'));
		$resAuthenLdap = $this->ldap_authen($user,$pass);
        $result = $this->User_model->validate($resAuthenLdap);

        if(! $result) {
            $this->session->set_flashdata('msg-danger', 'username or password incorrect.');
            redirect("login","refresh");

        } else {

            // When Login Success
            $session_data = $this->session->userdata('logged_in_user');
            // Send To Dashboard
            redirect('timesheet/view',"refresh");
        }
    }

    public function auth_user_login_ajax() {

        // grab user input
        $sUserName  = $this->security->xss_clean($this->input->post('sUserName'));
        $sPassword  = $this->security->xss_clean($this->input->post('sPassword'));
        $sTel       = $this->security->xss_clean($this->input->post('sTel'));

        $result = $this->User_model->validate_login($sUserName,$sPassword,$sTel);

        if(! $result) {

            echo json_encode(array("status" => 0,"msg" => "รหัสนักศึกษา หรือ รหัสผ่าน ผิดพลาด กรุณากรอกใหม่อีกครั้ง"));

        } else {
            // When Login Success
            $session_data = $this->session->userdata('logged_in_user');
            echo json_encode(array("status" => 1,"msg" => "","url"=> "member/register_quota"));
        }

    }

    public function logout_user() {
        $this->session->unset_userdata('logged_in_user');
        redirect("login","refresh");
    }



}
