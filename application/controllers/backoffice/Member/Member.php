<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct(){
    parent::__construct();

		if(empty($this->session->userdata('userdata_admin'))){
			$this->session->sess_destroy();
			redirect(base_url("helpdesk/admin"));
		}

		$this->dCurrent = date('Y-m-d H:i:s');
    $this->load->model('Member_model');
    $this->load->model('Register_model');

  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$aUserData = $this->session->userdata('userdata_admin');
		$this->data["aData"] = $this->Member_model->getMember();
		$this->load->view('backoffice/member/index',$this->data);
	}

	public function form()
	{
		$aUserData = $this->session->userdata('userdata_admin');
		$user_id = $this->input->get('user_id');

		$this->data['rData'] = $this->Member_model->getRowUserByUserID($user_id);
		$this->data["aDept"] = $this->Register_model->getDept();

		$this->load->view('backoffice/member/form',$this->data);
	}

	public function save()
	{
		$aData = array(
			'username' 			=> htmlspecialchars($this->input->post('sUsername')),
			'password' 			=> md5($this->input->post('sPassword')),
			'firstname' 		=> htmlspecialchars($this->input->post('sFirstname')),
			'lastname' 			=> htmlspecialchars($this->input->post('sLastname')),
			'computer_name' => htmlspecialchars($this->input->post('sCompName')),
			'department_id' => htmlspecialchars($this->input->post('iDept')),
			'tel' 					=> htmlspecialchars($this->input->post('sTel')),
			'user_email' 		=> $this->input->post('user_email'),
			'createdate'		=>	$this->dCurrent,
			'updatedate'		=>	$this->dCurrent,
			'deleted'				=>	0,
		);
		$this->load->model('Register_model');
		$aUsername = $this->Register_model->chkDupUsername(htmlspecialchars($this->input->post('sUsername')));
		if(empty($aUsername)){
			$this->Register_model->save($aData);
			$this->session->set_flashdata('msg-success','Success, Thank you.');
		}else{
			$this->session->set_flashdata('msg-danger','Error, Please try again because username duplicate.');
		}
		redirect(base_url("helpdesk/admin_issue/member/form"),'refresh');
	}


	public function cancel()
	{
		$aUserData = $this->session->userdata('userdata_admin');
		$issue_id = $this->input->get('issue_id');
		$aData = array(
			'repair_by' => $aUserAdmin['firstname']." ".$aUserAdmin['lastname'],
			'status' => 2, // 2 = cancel
			'updatedate' => date('Y-m-d H:i:s'),
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);
		redirect($this->agent->referrer(),'refresh');
	}

	public function success()
	{
		$aUserAdmin = $this->session->userdata('userdata_admin');
		$issue_id = $this->input->get('issue_id');
		$aData = array(
			'repair_by' => $aUserAdmin['firstname']." ".$aUserAdmin['lastname'],
			'status' => 1, // 2 = cancel
			'received_date' => date('Y-m-d'),
			'updatedate' => date('Y-m-d H:i:s'),
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);
		redirect($this->agent->referrer(),'refresh');
	}

	public function in_progress()
	{
		$aUserAdmin = $this->session->userdata('userdata_admin');
		$issue_id = $this->input->get('issue_id');
		$aData = array(
			'repair_by' => $aUserAdmin['firstname']." ".$aUserAdmin['lastname'],
			'status' => 3, // 2 = in_progress
			'received_date' => date('Y-m-d'),
			'updatedate' => date('Y-m-d H:i:s'),
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);
		redirect($this->agent->referrer(),'refresh');
	}

	public function repair()
	{
		$issue_id = $this->input->get('issue_id');
		$this->data["aData"] = $this->Issue_model->getIssueByID($issue_id);
		$this->load->view('backoffice/helpdesk/form',$this->data);
	}

	public function view()
	{
		$this->load->model('Home_model');
		$issue_id = $this->input->get('issue_id');
		$this->data['aData']	=	$this->Home_model->getIssueByID($issue_id);
		$this->load->view('backoffice/helpdesk/view',$this->data);
	}

	public function update()
	{
		$issue_id	= $this->input->post('issue_id');
		$aData = array(
			'case_of_problem_it'	=> $this->input->post('case_of_problem_it'),
			'corrective_action_detail'	=> $this->input->post('corrective_action_detail'),
			'corrective_action'	=> $this->input->post('corrective_action'),
			'corrective_action_remark'	=> $this->input->post('corrective_action_remark'),
			'finished_date'	=> $this->input->post('finished_date'),
			'repair_by'	=> $this->input->post('repair_by'),
			'preventive_action'	=> $this->input->post('preventive_action'),
			'updatedate' => date('Y-m-d H:i:s'),
		);
		$this->db->where('issue_id',$issue_id);
		$this->db->update('issue_tbl',$aData);
		redirect(base_url("helpdesk/admin_issue/list"),'refresh');
	}

	public function getIssueByID()
	{
		$issue_id = $this->input->post('issue_id');
		$aData = $this->Issue_model->getIssueByID($issue_id);
		$aSerial = unserialize($aData["feedback"]);
		$rData = array('feedback'=>$aSerial);
		echo json_encode($aSerial);
		// $issue_id	=	$this->input->post('issue_id');
	}

}
