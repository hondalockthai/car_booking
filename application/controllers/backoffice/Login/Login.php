<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->dCurrent = date('Y-m-d H:i:s');
		$this->load->model('Admin_login_model');

	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('backoffice/login/login');
	}

	public function authen()
	{
		$sUsername 	= htmlspecialchars($this->input->post('sUsername'));
		$sPassword 	= htmlspecialchars(md5($this->input->post('sPassword')));
		$aData 			= $this->Admin_login_model->authen($sUsername,$sPassword);
		if(!empty($aData)){
			$this->session->set_userdata('userdata_admin',$aData);
			redirect(base_url("helpdesk/admin_issue/list"),'refresh');
		}else{
			$this->session->set_flashdata('msg-danger','Username or password is incorrect');
			redirect(base_url("helpdesk/admin"),'refresh');
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(base_url("helpdesk/admin"),'refresh');
	}
}
