<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct(){
    parent::__construct();

		if(empty($this->session->userdata('userdata_admin'))){
			$this->session->sess_destroy();
			redirect(base_url("helpdesk/admin"));
		}

		$this->dCurrent = date('Y-m-d H:i:s');
    $this->load->model('Issue_model');
    $this->load->model('Report_model');

  }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('Report_model');
		$this->load->view('backoffice/report/index');
	}

	public function detail()
	{
		$aUserData = $this->session->userdata('userdata_admin');
		$this->data["aData"] = $this->Issue_model->getIssueList();
		$this->load->view('backoffice/report/detail',$this->data);
	}

	public function getIssueByID()
	{
		$issue_id = $this->input->post('issue_id');
		$aData = $this->Issue_model->getIssueByID($issue_id);
		$aSerial = unserialize($aData["feedback"]);
		$rData = array('feedback'=>$aSerial);
		echo json_encode($aSerial);
		// $issue_id	=	$this->input->post('issue_id');
	}

	public function insert_dep()
	{
		echo "Please check code";
		die();
		$array = array(
'Die Casting'
,'Plastic Operation'
,'Painting'
,'Assembly 2'
,'Assembly 1'
,'Assembly 2'
,'Production Control'
,'Procurement'
,'Manufacturing Engineering'
,'Die Making'
,'Mass Production Quality Assurance'
,'New Model Quality'
,'Parts  Quality'
,'Human Resource & General Affairs'
,'ISO Center'
,'Safety&Environmental'
,'TQM Promotion'
,'Accounting'
,'New Model Promotion'
,'Sale'
,'Cost&Sourcing');
foreach ($array as $key => $value) {
	$arr = array(
		'department_name' => $value,
		'createdate' => $this->dCurrent,
		'updatedate' => $this->dCurrent,
		'deleted' => 0,
	);
	$this->db->insert('department_tbl',$arr);
	echo $this->db->insert_id()."<br>";
}

	}

}
