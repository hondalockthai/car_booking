<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sendingemail_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');

    }
    public function index() {
        $this->load->helper('form');
        $this->load->view('email_view');
    }

    public function updateUserTbl()
    {

        $this->load->model('Member_model');

        $host = "ldap://10.169.128.152";
        $user = 'hlt_admin';
        // $user = "sornram_sukpi";
        $baseDn1 = 'hl\\hlt_admin';
        $baseDn2 = 'OU=HLT_Local,OU=HLT_Users,OU=HLT,DC=hl,DC=domain';
        $password = 'Hlt123456';
        // $password = 'SoSu#8541';
        $ldap = @ldap_connect($host);
        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        $b = @ldap_bind($ldap,$baseDn1,$password);
        if($b){
          // echo "pass<br>";

          $result = ldap_search($ldap, $baseDn2,"sAMAccountName=*");
          // $result = ldap_search($ldap, $baseDn2,"sAMAccountName=srisak_anusu");
          $rec = ldap_get_entries($ldap,$result);
          foreach ($rec as $key => $value) {
            if(!empty($value["department"][0]) && !empty($value["mail"][0])){

              // echo $value["givenname"][0].' - '.$value["department"][0].'<br>';
              $aMailUser = explode('@',$value["mail"][0]);
              $user = $aMailUser[0];
              // echo $user.'<br>';
              $dept_id = '';
              if(!empty($value["department"][0])){
                $getDept = $this->Member_model->getDeptByDeptCode($value["department"][0]);
                $dept_id = $getDept['department_id'];
              }

              $rData = $this->Member_model->getDataByUsername($user);
              // echo "<pre>";
              // print_r($rData);
              // die();
              if(!empty($rData)){
                // UpdateDataUser
                $aData = array(
                  'password' => '',
                  'firstname' => (!empty($value["givenname"][0]))?$value["givenname"][0]:'',
                  'lastname' => (!empty($value["sn"][0]))?$value["sn"][0]:'',
                  'tel' => (!empty($value["telephonenumber"][0]))?$value["telephonenumber"][0]:'',
                  'department_id' => $dept_id,
                  'user_email' => (!empty($value["mail"][0]))?$value["mail"][0]:'',
                  'is_manager' => (!empty($value["title"][0]) && $value["title"][0] == 'Manager')?'1':'0',
                  'updatedate' => date('Y-m-d H:i:s'),
                );
                $this->db->where('username',$user);
                $this->db->update('user_tbl',$aData);
              }else{
                // InsertDataUser
                $aData = array(
                  'username' => $user,
                  'password' => '',
                  'firstname' => (!empty($value["givenname"][0]))?$value["givenname"][0]:'',
                  'lastname' => (!empty($value["sn"][0]))?$value["sn"][0]:'',
                  // 'computer_name' => ,
                  'department_id' => $dept_id,
                  'tel' => (!empty($value["telephonenumber"][0]))?$value["telephonenumber"][0]:'',
                  'user_email' => (!empty($value["mail"][0]))?$value["mail"][0]:'',
                  'is_manager' => (!empty($value["title"][0]) && $value["title"][0] == 'Manager')?'1':'0',
                  'createdate' => date('Y-m-d H:i:s'),
                  'updatedate' => date('Y-m-d H:i:s'),
                  'deleted' => 0,
                );
                $this->db->insert('user_tbl',$aData);
              }


          }

        }
      }
      ldap_unbind($ldap);
    }

    public function send_mail() {

      $sEmail = 'sornram_sukpi@hlt.co.th';
      $this->load->library('MY_Email');
      $this->email->from('sornram_sukpi@hlt.co.th');
      $this->email->to(
            array($sEmail,
            )
        );
      //   // $this->email->cc();
      $this->load->model('Home_model');
  		$issue_id = 1;
  		// $issue_id = $this->input->get('issue_id');
  		$aData	=	$this->Home_model->getIssueByID($issue_id);
      $aSerial = unserialize($aData['type_of_req']);
      // $htmlContent = 'Dear ............................</br>';
      $htmlContent = '<br>';
      $htmlContent .= '<center>
      <h3>
        <strong class="text-danger pull-left" style="color:red;">Honda Lock</strong>
        <strong>แบบบันทึกการแจ้งซ่อม Hardware, Software และ Network</strong>
        <span class="pull-right"><strong>Issue No : </strong><u>'.sprintf("%05d",$aData["issue_id"]).'</u></span>
      </h3>
      <hr>
<center>
      <table width="85%">
        <tbody>
          <tr>
            <td width="33%"></td>
            <td width="15%" height="30px"><b>Name of Requster :</b></td>
            <td>'.$aData["name_of_req"].'</td>
          </tr>
          <tr>
            <td width="33%"></td>
            <td width="15%" height="30px"><b>Computer name :</b></td>
            <td>'.$aData["com_name"].'</td>
          </tr>
          <tr>
            <td width="33%"></td>
            <td width="15%" height="30px"><b>Department :</b></td>
            <td>'.$aData["dept_name"].'</td>
          </tr>
          <tr>
            <td width="33%"></td>
            <td width="15%" height="30px"><b>Phone No. :</b></td>
            <td>'.$aData["phone_no"].'</td>
          </tr>
          <tr>
            <td width="33%"></td>
            <td width="15%" height="30px"><b>Type of request :</b></td>
            <td>'.json_encode($aSerial).'<</td>
          </tr>
          <tr>
            <td width="33%"></td>
            <td width="15%" height="30px"><b>Case of Problem :</b></td>
            <td>'.$aData["case_of_problem"].'</td>
          </tr>
        </tbody>
      </table>
  </center>
      <hr>
      <p><a href="'.base_url("helpdesk/login").'" target="_blank">'.base_url("helpdesk/login").'</a></p>
</center>';
        $this->email->subject("Issue No. 00001");
        $this->email->message($htmlContent);
      //
        if(!empty($sEmail)){
          if($this->email->send()){
            echo 'OK';
          }else{
            echo 'OK';
          }
        }
    }
    public function send_mail2() {

      // $this->load->view('email_send');

      // die();
      $urlBase = 'localhost/';
			//Load email library
			$this->load->library('email');
      $sEmail = $this->input->post('email');
      $sEmail = 'sornram_sukpi@hlt.co.th';
				//SMTP & mail configuration
        // svex01.hl.domain
				$config = array(
				    'protocol'  => 'smtp',
				    'smtp_host' => '10.169.52.146',
				    'smtp_port' => 25,
				    'smtp_user' => 'sornram_sukpi',
				    'smtp_pass' => 'SoSu#8541',
				    'mailtype'  => 'html',
				    'charset'   => 'utf-8');
						$this->email->initialize($config);
						// $this->email->set_mailtype("html");
						$this->email->set_newline("\r\n");
				//Email content

        // $body="<a href='http://localhost/helpdesk/approve?token=QWESDFXCV'>Approve</a></br>";


				$htmlContent = 'Dear ............................</br>';
				$htmlContent .= '</br>See detail <a href="http://localhost/helpdesk/login">http://localhost/helpdesk/login</a></br>';
				$htmlContent .= '<hr/><b>IT</b>';
				$htmlContent .= '</br><b style="color:red;">Honda Lock</b>';

				$this->email->to($sEmail);
				$this->email->from('sornram_sukpi@hlt.co.th','SysAdmin');
        $htmlTopic = 'Test Send e-mail '.date('YmdHis').'';
				$this->email->subject($htmlTopic);
				$this->email->message($htmlContent);
        //Send mail
        if($this->email->send())
            $this->session->set_flashdata("email_sent","Congragulation Email Send Successfully.");
        else
            $this->session->set_flashdata("email_sent","You have encountered an error");
        $this->load->view('email_view');
    }

    public function ldap_authen()
    {
      $this->load->model('Member_model');

      $host = "ldap://10.169.128.152";
      $user = $this->input->post('sUsername');
      // $user = "sornram_sukpi";
      $baseDn1 = 'hl\\'.$user;
      $baseDn2 = 'OU=HLT_Local,OU=HLT_Users,OU=HLT,DC=hl,DC=domain';
      $password = $this->input->post('sPassword');
      // $password = 'SoSu#8541';
      $ldap = @ldap_connect($host);
      ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
      $b = @ldap_bind($ldap,$baseDn1,$password);
      if($b){
        // echo "pass<br>";

        $result = ldap_search($ldap, $baseDn2,"sAMAccountName=$user");
        // $result = ldap_search($ldap, $baseDn2,"sAMAccountName=srisak_anusu");
        $rec = ldap_get_entries($ldap,$result);
        // echo "<pre>";
        // print_r($rec);
        // die();
        // echo $rec[0]["department"][0];
        $dept_id = '';
        if(!empty($rec[0]["department"][0])){
          $getDept = $this->Member_model->getDeptByDeptCode($rec[0]["department"][0]);
          $dept_id = $getDept['department_id'];
        }

        $rData = $this->Member_model->getDataByUsername($user);

        if(!empty($rData)){
          // UpdateDataUser
          $aData = array(
            'password' => md5($password),
            'firstname' => (!empty($rec[0]["givenname"][0]))?$rec[0]["givenname"][0]:'',
            'lastname' => (!empty($rec[0]["sn"][0]))?$rec[0]["sn"][0]:'',
            'department_id' => $dept_id,
            'user_email' => (!empty($rec[0]["mail"][0]))?$rec[0]["mail"][0]:'',
            // 'is_manager' => (!empty($rec[0]["title"][0]) && $rec[0]["title"][0] == 'Manager')?'1':'0',
            'updatedate' => date('Y-m-d H:i:s'),
          );
          $this->db->where('username',$user);
          $this->db->update('user_tbl',$aData);
        }else{
          // InsertDataUser
          $aData = array(
            'username' => $user,
            'password' => md5($password),
            'firstname' => (!empty($rec[0]["givenname"][0]))?$rec[0]["givenname"][0]:'',
            'lastname' => (!empty($rec[0]["sn"][0]))?$rec[0]["sn"][0]:'',
            // 'computer_name' => ,
            'department_id' => $dept_id,
            'tel' => (!empty($rec[0]["telephonenumber"][0]))?$rec[0]["telephonenumber"][0]:'',
            'user_email' => (!empty($rec[0]["mail"][0]))?$rec[0]["mail"][0]:'',
            'is_manager' => (!empty($rec[0]["title"][0]) && $rec[0]["title"][0] == 'Manager')?'1':'0',
            'createdate' => date('Y-m-d H:i:s'),
            'updatedate' => date('Y-m-d H:i:s'),
            'deleted' => 0,
          );
          $this->db->insert('user_tbl',$aData);
        }

        ldap_unbind($ldap);

      }else{

      }
      $this->load->model('Login_model');
      $aData 			= $this->Login_model->authen($user,md5($password));
      if(!empty($aData)){
        $this->session->set_userdata('userdata',$aData);
        redirect(base_url("helpdesk/home"),'refresh');
      }else{
        $this->session->set_flashdata('msg-danger','Username or password is incorrect');
        redirect(base_url("helpdesk/login"),'refresh');
      }
    }

    public function usergenerate()
    {
      $this->db->select('*');
      $this->db->from('department_tbl');
      $this->db->where('deleted',0);
      $qry = $this->db->get();
      $aDept = $qry->result_array();
      echo '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
      <br><h2 class="text-center">User for test</h2><br><center>
        <table class="table table-bordered table-hover table-striped" style="width:50% !important">
          <thead class="thead-dark">
              <tr>
                <th>Username</th>
                <th>Password</th>
                <th>Department ID</th>
                <th>Department Name</th>
                <th>Department Code</th>
                <th>Test Send to Email</th>
              </tr>
          </thead>
          <tbody>
      ';
      foreach ($aDept as $kDept => $vDept) {
        $cnt = ($kDept+1);

        if($cnt < 10){
          $cnt = '0'.$cnt;
        }

        $sql = "INSERT INTO `user_tbl` (`user_id`, `username`, `password`, `firstname`, `lastname`, `computer_name`, `department_id`, `tel`, `user_email`, `is_manager`, `createdate`, `updatedate`, `deleted`) VALUES (NULL, 'user".$cnt."', MD5('1234'), 'Chanittha', 'lompla', '', '".$vDept["department_id"]."', '', 'chanittha.l@hlt.co.th', '1', '2019-06-14 11:34:05', '2019-06-18 14:35:38', '0')";
        // echo $sql."<br>";
        // die();
        // $this->db->query($sql);
        // die();
        echo '
        <tr>
          <td>user'.$cnt.'</td>
          <td>1234</td>
          <td>'.$vDept["department_id"].'</td>
          <td>'.$vDept["department_name"].'</td>
          <td>'.$vDept["department_code"].'</td>
          <td>chanittha.l@hlt.co.th</td>';
        echo '</tr>
        ';
      }
      echo '
      </tbody></table></center>
      <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      ';
      // echo "<pre>";
      // print_r($aDept);
      die();
    }

}
