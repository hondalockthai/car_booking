<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Issue_model extends CI_Model {

  public function __construct() {
      parent::__construct();
      $this->tableCore        = "issue_tbl";
      $this->_pkKeyCore       = "issue_id";
  }

  public function save($data) {
      $this->db->insert($this->tableCore, $data);
      return $this->db->insert_id();
  }

  public function update($iId,$data) {
      $this->db->where($this->_pkKeyCore, $iId);
      $this->db->update($this->tableCore, $data);
      return $this->db->affected_rows();
  }

  public function delete($iId) {
      // Set Date Time Current
      $dCurrent = date("Y-m-d H:i:s");

      $this->db->set('deleted',1);
      $this->db->set('updatedate',$dCurrent);
      $this->db->where($this->_pkKeyCore,$iId);
      $this->db->update($this->tableCore);
      return $this->db->affected_rows();
  }

  public function getIssueList()
  {
    $this->db->select('*');
    $this->db->from($this->tableCore);
    $this->db->order_by('createdate','desc');
    $qry = $this->db->get();
    return $qry->result_array();
  }

  public function getMyIssue($user_id)
  {
    $this->db->select('*');
    $this->db->from($this->tableCore);
    $this->db->where('user_id',$user_id);
    $this->db->order_by('createdate','desc');
    $qry = $this->db->get();
    return $qry->result_array();
  }

  public function getDeptByid($dept_id)
  {
    $this->db->select('*');
    $this->db->from('department_tbl');
    $this->db->where('department_id',$dept_id);
    $qry = $this->db->get();
    return $qry->row_array();
  }

  public function getIssueByID($issue_id)
  {
    $this->db->select('*');
    $this->db->from('issue_tbl');
    $this->db->where('issue_id',$issue_id);
    $qry = $this->db->get();
    return $qry->row_array();
  }

}
