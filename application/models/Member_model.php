<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Member_model extends CI_Model {

  public function __construct() {
      parent::__construct();
      $this->tableCore        = "submember_tbl";
      $this->_pkKeyCore       = "user_id";
  }

  public function save($data) {
      $this->db->insert($this->tableCore, $data);
      return $this->db->insert_id();
  }

  public function saveReservaion($data) {
      $this->db->insert("reservation_tbl", $data);
      return $this->db->insert_id();
  }

  public function update($iId,$data) {
      $this->db->where($this->_pkKeyCore, $iId);
      $this->db->update($this->tableCore, $data);
      return $this->db->affected_rows();
  }

  public function setDeleted_1($member_id,$reserv_date,$time_id,$aDataUpdate) {
      $this->db->where('reservation_tbl.member_id', $member_id);
      $this->db->where('reservation_tbl.reserv_date', $reserv_date);
      $this->db->where('reservation_tbl.time_id', $time_id);
      $this->db->update('reservation_tbl', $aDataUpdate);
      return $this->db->affected_rows();
  }

  public function delete($iId) {
      // Set Date Time Current
      $dCurrent = date("Y-m-d H:i:s");

      $this->db->set('deleted',1);
      $this->db->set('updatedate',$dCurrent);
      $this->db->where($this->_pkKeyCore,$iId);
      $this->db->update($this->tableCore);
      return $this->db->affected_rows();
  }

  public function getRowUserByUserID($user_id)
  {
    $this->db->select('*');
    $this->db->from($this->tableCore);
    $this->db->where('user_id',$user_id);
    $qry = $this->db->get();
    return $qry->row_array();
  }

  public function getsupmember($memb_id)
  {
    $this->db->select('*');
    $this->db->from('submember_tbl');
    $this->db->where('submember_tbl.memb_id',$memb_id);
    $qry = $this->db->get();
    return $qry->result_array();
  }

  public function getUserMyDept($dept_id)
  {
    $this->db->select('*');
    $this->db->from('user_tbl');
    $this->db->join('buspoint_tbl','buspoint_tbl.id = user_tbl.buspoint_id','left');
    $this->db->join('bus_tbl','bus_tbl.id = buspoint_tbl.idbus','left');
    $this->db->where('user_tbl.dept_id',$dept_id);
    $qry = $this->db->get();
    return $qry->result_array();
  }

  public function getTime()
  {
    $this->db->select('*');
    $this->db->from('time_tbl');
    $this->db->where('time_tbl.deleted',0);
    $this->db->order_by('id','asc');
    $qry = $this->db->get();
    return $qry->result_array();
  }

  public function getTimeById($time_id)
  {
    $this->db->select('*');
    $this->db->from('time_tbl');
    $this->db->where('time_tbl.id',$time_id);
    $this->db->where('time_tbl.deleted',0);
    $qry = $this->db->get();
    return $qry->row_array();
  }

  public function getBus_All()
  {
    $this->db->select('*');
    $this->db->from('bus_tbl');
    $this->db->where('deleted', 0);
    $this->db->order_by('bus_name','asc');
    $qry = $this->db->get();
    return $qry->result_array();
  }

}
