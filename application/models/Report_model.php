<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_model extends CI_Model {

  public function __construct() {
      parent::__construct();
      $this->tableCore        = "issue_tbl";
      $this->_pkKeyCore       = "issue_id";
  }

  public function getYearIssue()
  {
    $this->db->select('year(createdate) as years');
    $this->db->from($this->tableCore);
    $this->db->group_by('years');
    $this->db->order_by('years','desc');
    $qry = $this->db->get();
    return $qry->result_array();
  }

  public function getIssueList()
  {
    $this->db->select('*');
    $this->db->from($this->tableCore);
    $qry = $this->db->get();
    return $qry->result_array();
  }

  public function getCnt($iMonth,$status)
  {
    $this->db->select('count(*) as cnt');
    $this->db->from($this->tableCore);
    if($status != 'All'){
      $this->db->where('status',$status);
    }
    $this->db->where('month(createdate)',$iMonth);
    $this->db->where('deleted',0);
    $qry = $this->db->get();
    return $qry->row_array();
  }

  public function getCntFeedback($iMonth,$status)
  {
    $this->db->select('count(*) as cnt,feedback');
    $this->db->from($this->tableCore);
    if($status != 'All'){
      $this->db->where('status',$status);
    }
    $this->db->where('month(createdate)',$iMonth);
    $this->db->where('feedback is not null');
    $this->db->where('deleted',0);
    $qry = $this->db->get();
    return $qry->row_array();
  }

  public function getCntByDept($iMonth,$status,$deptName)
  {
    $this->db->select('count(*) as cnt');
    $this->db->from($this->tableCore);
    if($status != 'All'){
      $this->db->where('status',$status);
    }
    $this->db->where('dept_name',$deptName);
    $this->db->where('month(createdate)',$iMonth);
    $this->db->where('deleted',0);
    $qry = $this->db->get();
    return $qry->row_array();
  }

  public function getDataCondd($iMonth,$status)
  {
    $this->db->select('*');
    $this->db->from($this->tableCore);
    $this->db->where('month(createdate)',$iMonth);
    $this->db->where('deleted',0);
    $qry = $this->db->get();
    return $qry->result_array();
  }

}
