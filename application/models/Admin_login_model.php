<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_login_model extends CI_Model {

  public function __construct() {
      parent::__construct();
      $this->tableCore        = "admin_tbl";
      $this->_pkKeyCore       = "admin_id";
  }

  public function save($data) {
      $this->db->insert($this->tableCore, $data);
      return $this->db->insert_id();
  }

  public function update($iId,$data) {
      $this->db->where($this->_pkKeyCore, $iId);
      $this->db->update($this->tableCore, $data);
      return $this->db->affected_rows();
  }

  public function delete($iId) {
      // Set Date Time Current
      $dCurrent = date("Y-m-d H:i:s");

      $this->db->set('deleted',1);
      $this->db->set('updatedate',$dCurrent);
      $this->db->where($this->_pkKeyCore,$iId);
      $this->db->update($this->tableCore);
      return $this->db->affected_rows();
  }
  public function authen($sUsername,$sPassword)
  {
		$this->db->select('*');
		$this->db->from($this->tableCore);
		$this->db->where('username',$sUsername);
		$this->db->where('password',$sPassword);
		$this->db->where('deleted',0);
		$qry = $this->db->get();
		return $qry->row_array();
  }

}
