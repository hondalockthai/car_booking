<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['email']               = 'Sendingemail_Controller/index';
$route['send_mail']           = 'Sendingemail_Controller/send_mail';
$route['updateuser']           = 'Sendingemail_Controller/updateUserTbl';
$route['ldap_authen']         = 'Sendingemail_Controller/ldap_authen';
$route['usergenerate']       = 'Sendingemail_Controller/usergenerate';
// $route['ldap_authen_test']           = 'Sendingemail_Controller/ldap_authen_test';

//User
$route['login']               = 'frontend/Login/Login/index';
$route['login/authen']        = 'frontend/Login/Login/authen';
// $route['login/authen']        = 'frontend/Login/Login/authen';
$route['logout']              = 'frontend/Login/Login/logout';
$route['feedback']            = 'frontend/Feedback/Feedback/update';

$route['register']            = 'frontend/Register/Register/index';
$route['register/save']       = 'frontend/Register/Register/save';

$route['home']                = 'frontend/Home/Home/index';

$route['reservation/step1']         = 'frontend/Reservation/Reservation/index';
$route['reservation/step2']         = 'frontend/Reservation/Reservation/step2';
$route['reservation/step3']         = 'frontend/Reservation/Reservation/step3';

$route['reservation/cancel']         = 'frontend/Reservation/Reservation/cancel_reserv';

$route['booking']                                           = 'frontend/Booking/Booking/index';
$route['booking/jsonviewdata']                              = 'frontend/Booking/Booking/jsonviewdata';
$route['booking/jsonviewdata/(:num)/(:num)/(:num)']         = 'frontend/Booking/Booking/jsonviewdata';

$route['member']                    = 'frontend/Member/Member/index';
$route['member/emp/(:num)']         = 'frontend/Member/Member/emp_buspoint';

$route['updatesubmember']         = 'frontend/Booking/Booking/updateuser';
$route['updatebuspoint']         = 'frontend/Booking/Booking/updatebuspoint';
$route['updateuser_dept']         = 'frontend/Booking/Booking/updateuser_dept';








//Admin
$route['admin']               = 'backoffice/Login/Login/index';
$route['admin/authen']        = 'backoffice/Login/Login/authen';
$route['admin_logout']        = 'backoffice/Login/Login/logout';

$route['admin_issue/list']                        = 'backoffice/Issue/Issue/index';
$route['admin_issue/view']                        = 'backoffice/Issue/Issue/view';
$route['admin_issue/repair']                      = 'backoffice/Issue/Issue/repair';
$route['admin_issue/cancel']                      = 'backoffice/Issue/Issue/cancel';
$route['admin_issue/success']                     = 'backoffice/Issue/Issue/success';
$route['feedback/getIssueByID']                   = 'backoffice/Issue/Issue/getIssueByID';
$route['admin_issue/in_progress']                 = 'backoffice/Issue/Issue/in_progress';
$route['admin_issue/update']                      = 'backoffice/Issue/Issue/update';

$route['admin_issue/report']                      = 'backoffice/Report/Report/index';
$route['admin_issue/report/detail']               = 'backoffice/Report/Report/detail';
$route['admin_issue/report/insert_dep']           = 'backoffice/Report/Report/insert_dep';

$route['admin_issue/member']                      = 'backoffice/Member/Member/index';
$route['admin_issue/member/form']                 = 'backoffice/Member/Member/form';

$route['admin_issue/member/save']                 = 'backoffice/Member/Member/save';
