<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masterlibs {

    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }

    /*
     * Upload Image Multiple File And Create Thumbnail
     * @return Array
     */
    public function upload_mulitple_image($aFileImage,$path) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/images/'.$path;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = '2048';
        //$config['encrypt_name']     = TRUE;
        $config['file_name']        = date("YmdHis");; // set the name here

        //$config['max_width']      = '1024';
        //$config['max_height']     = '768';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        // Change $_FILES to new vars and loop them
        foreach($aFileImage as $key=>$val) {
            $i = 1;
            foreach($val as $v) {
                $field_name = "file_".$i;
                $_FILES[$field_name][$key] = $v;
                $i++;
            }
        }

        // Unset the useless one ;)
        unset($_FILES['sFileImage']);

        $aFileUpload = array();
        $error = array();

        // main action to upload each file
        foreach($_FILES as $field_name => $file) {

            // if ( ! $this->CI->upload->do_upload($field_name)) {
            //     $error = array('error' => $this->CI->upload->display_errors());
            //     $this->CI->session->set_flashdata('error', $error["error"]);
            // } else {
            //     $fResizeImage           = $this->resizeImage($this->CI->upload->data());
            //     if($fResizeImage) {
            //         $aFileUpload[]      = $this->CI->upload->data();
            //     }
            // }

            if ( ! $this->CI->upload->do_upload($field_name)) {

                $aFileUpload[] = array('error' => $this->CI->upload->display_errors(),'status' => 0);
                //return $error;

            } else {

            // $aFileUpload              = array('success' => $this->CI->upload->data(),'status' => 1);
            // return $aFileUpload;

                $fResizeImage           = $this->resizeImage($this->CI->upload->data());
                if($fResizeImage) {
                    $aFileUpload[]      =  array('success' => $this->CI->upload->data(),'status' => 1);

                }


            }

          //return $aFileUpload;

        }

        return $aFileUpload;

    } // End Function Upload Image Multiple

     public function upload_newsmulitple_image($aFileImage,$path) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/images/'.$path;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = '2048';
        $config['max_width']      = '1640';
        $config['max_height']     = '820';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        // Change $_FILES to new vars and loop them
        foreach($aFileImage as $key=>$val) {
            $i = 1;
            foreach($val as $v) {
                $field_name = "file_".$i;
                $_FILES[$field_name][$key] = $v;
                $i++;
            }
        }

        // Unset the useless one ;)
        unset($_FILES['sFileImage']);

        $aFileUpload = array();

        if(empty($aFileUpload)){
            // main action to upload each file
            foreach($_FILES as $field_name => $file) {
            $sizeimage = list($config['width'],  $config['height']) = getimagesize($file['tmp_name']);
                if($config['width'] <= 1640 && $config['height'] <= 820){
                    if ( ! $this->CI->upload->do_upload($field_name)) {
                        $error = array('error' => $this->CI->upload->display_errors());
                        $this->CI->session->set_flashdata('error', $error["error"]);
                    } else {
                        $fResizeImage           = $this->resizeNewsImage($this->CI->upload->data());
                        if($fResizeImage) {
                            $aFileUpload[]      = $this->CI->upload->data();
                        }

                    }
                }
            }

             return $aFileUpload;
        }



    } // End Function Upload Image Multiple


    /*
     * Upload Single Image
     */
    public function upload_image($path) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/images/'.$path;
        $config['allowed_types']    = 'gif|jpg|png';
        $config['max_size']         = '2048';
        $config['file_name']        = date("YmdHis");; // set the name here
        //$config['max_width']      = '1024';
        //$config['max_height']     = '768';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);
        $aFileUpload = array();

        if ( ! $this->CI->upload->do_upload("sFileImage")) {
            $error = array('error' => $this->CI->upload->display_errors());
            $this->CI->session->set_flashdata('error', $error["error"]);
            return false;
        } else {
            $fResizeImage           = $this->resizeImage($this->CI->upload->data());
            if($fResizeImage) {
                $aFileUpload        = $this->CI->upload->data();
            }
        }

        return $aFileUpload;

    }

    /*
     * Resize Image
     */
    public function resizeImage($aImageData) {
        $config = array();
        $config['image_library']    = 'gd2';
        $config['source_image']     = $aImageData["full_path"];
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = TRUE;
        $config['width']            = 150;
        $config['height']           = 150;

        $this->CI->load->library('image_lib', $config);
        $this->CI->image_lib->initialize($config);

        if ( ! $this->CI->image_lib->resize()) {
            echo $this->CI->image_lib->display_errors();
            return false;
        } else {
            $this->CI->image_lib->resize();
            $this->CI->image_lib->clear();
            unset($config);
            return true;
        }
    }

      /*
     * Resize New Image
     */
    public function resizeNewsImage($aImageData) {
        $config = array();
        $config['image_library']    = 'gd2';
        $config['source_image']     = $aImageData["full_path"];
        $config['create_thumb']     = TRUE;
        $config['maintain_ratio']   = TRUE;
        $config['width']            = 820;
        $config['height']           = 410;

        $this->CI->load->library('image_lib', $config);
        $this->CI->image_lib->initialize($config);

        if ( ! $this->CI->image_lib->resize()) {
            echo $this->CI->image_lib->display_errors();
            return false;
        } else {
            $this->CI->image_lib->resize();
            $this->CI->image_lib->clear();
            unset($config);
            return true;
        }
    }

    public function upload_document_singlefile($file,$path) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/docs/'.$path;
        $config['allowed_types']    = 'doc|docx|xls|xlsx|pdf|jpg|png';
        $config['max_size']         = '2048';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        if ( ! $this->CI->upload->do_upload($file)) {

            $error = array('error' => $this->CI->upload->display_errors());
            return $error;

        } else {
            //$aData                    = array('upload_data' => $this->CI->upload->data());
            $aFileUpload              = $this->CI->upload->data();
        }
        return $aFileUpload;

    }

    public function upload_single_image($file,$path) {
        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/images/'.$path;
        $config['allowed_types']    = 'JPG|JPEG|jpg|jpeg|png';
        $config['max_size']         = '2048';
        $config['file_name']        = date("YmdHis");; // set the name here
        // $config['max_width']        = '1920';
        // $config['max_height']       = '1440';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        if ( ! $this->CI->upload->do_upload($file)) {
            $error          = array('status' => 0,'error' => $this->CI->upload->display_errors());
            return $error;
        } else {
            $fResizeImage           = $this->resizeImageWH($this->CI->upload->data(),450,350);
            if($fResizeImage) {
                $aFileUpload    = array('status' => 1,'success' => $this->CI->upload->data());
                return $aFileUpload;
            }
        }
    }


    /**
    * Upload File Document
    */
    public function upload_document_file($file,$path) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/docs/'.$path;
        $config['allowed_types']    = 'doc|docx|xls|xlsx|pdf|jpg|png';
        $config['max_size']         = '2048';
        $config['file_name']        = date("YmdHis");; // set the name here
        // $config['max_width']        = '1920';
        // $config['max_height']       = '1440';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        if ( ! $this->CI->upload->do_upload($file)) {

            $error = array('status' => 0,'error' => $this->CI->upload->display_errors());
            return $error;

        } else {

            //$fResizeImage           = $this->resizeImageWH($this->CI->upload->data(),250,300);

            //if($fResizeImage) {
            //$aFileUpload        = $this->CI->upload->data();
            $aFileUpload        = array('status' => 1,'success' => $this->CI->upload->data());
            //}
            return $aFileUpload;
        }

    }

    /**
    * Upload Banner
    */
    public function upload_image_banner($file,$path) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/images/'.$path;
        $config['allowed_types']    = 'jpg|png';
        $config['max_size']         = '1024';
        $config['file_name']        = date("YmdHis");; // set the name here
        $config['max_width']        = '1904';
        $config['max_height']       = '350';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        if ( ! $this->CI->upload->do_upload($file)) {
            $error          = array('status' => 0,'error' => $this->CI->upload->display_errors());
            return $error;
        } else {
            $aFileUpload    = array('status' => 1,'success' => $this->CI->upload->data());
            return $aFileUpload;
        }

    }

    /**
    * Upload Limit Size
    */
    public function upload_image_limit_size($file,$path,$width,$height) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/images/'.$path;
        $config['allowed_types']    = 'jpg|png';
        $config['max_size']         = '2048';
        $config['file_name']        = date("YmdHis");; // set the name here
        $config['max_width']        = $width;
        $config['max_height']       = $height;

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        if ( ! $this->CI->upload->do_upload($file)) {
            $error          = array('status' => 0,'error' => $this->CI->upload->display_errors());
            return $error;
        } else {

            $aFileUpload    = array('status' => 1,'success' => $this->CI->upload->data());
            return $aFileUpload;
        }

    }

    public function upload_image_limit_size_resize($file,$path,$width,$height,$resize_width,$resize_height) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/images/'.$path;
        $config['allowed_types']    = 'jpg|png';
        $config['max_size']         = '2048';
        $config['file_name']        = date("YmdHis");; // set the name here
        $config['max_width']        = $width;
        $config['max_height']       = $height;

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        if ( ! $this->CI->upload->do_upload($file)) {
            $error          = array('status' => 0,'error' => $this->CI->upload->display_errors());
            return $error;
        } else {
            $fResizeImage           = $this->resizeImageWH($this->CI->upload->data(),$resize_width,$resize_height);
            if($fResizeImage) {
                $aFileUpload    = array('status' => 1,'success' => $this->CI->upload->data());
                return $aFileUpload;
            }
        }

    }


    /*
     * Resize Image
     */
    public function resizeImageWH($aImageData,$width,$hight) {
        $config = array();
        $config['image_library']    = 'gd2';
        $config['source_image']     = $aImageData["full_path"];
        $config['create_thumb']     = TRUE;
        //$config['maintain_ratio']   = TRUE;
        $config['width']            = $width;
        $config['height']           = $hight;

        $this->CI->load->library('image_lib', $config);
        $this->CI->image_lib->initialize($config);

        if ( ! $this->CI->image_lib->resize()) {
            echo $this->CI->image_lib->display_errors();
            return false;
        } else {
            $this->CI->image_lib->resize();
            $this->CI->image_lib->clear();
            unset($config);
            return true;
        }
    }

    public function upload_image_slide($file,$path) {
        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/images/'.$path;
        $config['allowed_types']    = 'jpg|png';
        $config['max_size']         = '2048';
        $config['max_width']        = '1920';
        $config['max_height']       = '1440';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        if ( ! $this->CI->upload->do_upload($file)) {

            $error = array('error' => $this->CI->upload->display_errors(),'status' => 0);
            return $error;

        } else {
            //$aData                    = array('upload_data' => $this->CI->upload->data());
            //$aFileUpload              = $this->CI->upload->data();
            $aFileUpload              = array('success' => $this->CI->upload->data(),'status' => 1);
            return $aFileUpload;
        }


    }

    /*
    * Upload Document
    * @return Array
    */
    public function upload_document($path) {

        $this->CI->load->library('upload');

        $config['upload_path']      = './assets/public/uploads/docs/'.$path;
        $config['allowed_types']    = 'doc|docx|xls|xlsx|pdf|jpg|png';
        $config['max_size']         = '2048';

        $this->CI->load->library('upload', $config);
        $this->CI->upload->initialize($config);

        // Change $_FILES to new vars and loop them
        foreach($_FILES[key($_FILES)] as $key=>$val) {
            $i = 1;
            foreach($val as $v) {
                $field_name = "file_".$i;
                $_FILES[$field_name][$key] = $v;
                $i++;
            }
        }

        // Unset the useless one ;)
        unset($_FILES[key($_FILES)]);

        $aFileUpload = array();

        // main action to upload each file
        foreach($_FILES as $field_name => $file) {

            if ( ! $this->CI->upload->do_upload($field_name)) {

                $error = array('error' => $this->CI->upload->display_errors());
                return $error;

            } else {
                //$aData                    = array('upload_data' => $this->CI->upload->data());
                $aFileUpload[]              = $this->CI->upload->data();
            }

        }

        return $aFileUpload;

    }

    public function formatDateThai($date) {

        // Function explode
        $explodeDateTime    = explode(" ",$date);

        //month
        $getDate            = $explodeDateTime[0];

        // Explode Date
        $aDate              = explode("-",$getDate);

        // $month
        $month      = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");

        //month
        $get_month  = $aDate[1];

        //year
        $year       = $aDate["0"]+543;

        return $aDate["2"]." ".$month[$get_month]." ".$year.", ".$explodeDateTime[1];

    }

    public function dateTimeTh($dDate) {
        $date = new DateTime($dDate);
        $sMonth = Array(
                        '01' => 'มกราคม',
                        '02' => 'กุมภาพันธ์',
                        '03' => 'มีนาคม',
                        '04' => 'เมษายน',
                        '05' => 'พฤษภาคม',
                        '06' => 'มิถุนายน',
                        '07' => 'กรกฏาคม',
                        '08' => 'สิงหาคม',
                        '09' => 'กันยายน',
                        '10' => 'ตุลาคม',
                        '11' => 'พฤศจิกายน',
                        '12' => 'ธันวาคม');
        $y = $date->format('Y') + 543;
        return $date->format('j').' '.$sMonth[$date->format('m')].' '.$y.' เวลา '.$date->format('H:i:s').' น.';
    }

    public function convertShortDateThai($dDate) {
        $date = new DateTime($dDate);
        $sMonth = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $y = $date->format('Y') + 543;
        return $date->format('j').' '.$sMonth[$date->format('m')].' '.$y;
    }

    public function convertDateTh($dDate) {
        $date = new DateTime($dDate);
        $sMonth = Array(
                        '01' => 'มกราคม',
                        '02' => 'กุมภาพันธ์',
                        '03' => 'มีนาคม',
                        '04' => 'เมษายน',
                        '05' => 'พฤษภาคม',
                        '06' => 'มิถุนายน',
                        '07' => 'กรกฏาคม',
                        '08' => 'สิงหาคม',
                        '09' => 'กันยายน',
                        '10' => 'ตุลาคม',
                        '11' => 'พฤศจิกายน',
                        '12' => 'ธันวาคม');
        $y = $date->format('Y') + 543;
        return $date->format('j').' '.$sMonth[$date->format('m')].' '.$y;
    }

    public function convertDateEn($dDate) {
        $date = new DateTime($dDate);
        $sMonth = Array(
                        '01' => 'January',
                        '02' => 'February',
                        '03' => 'March',
                        '04' => 'April',
                        '05' => 'May',
                        '06' => 'June',
                        '07' => 'July',
                        '08' => 'August',
                        '09' => 'September',
                        '10' => 'October',
                        '11' => 'November',
                        '12' => 'December'
                    );
        $y = $date->format('Y');
        return $date->format('j').' '.$sMonth[$date->format('m')].' '.$y;
    }

    public function convertDateCn($dDate) {
        $date = new DateTime($dDate);
        $sMonth = Array(
                        '01' => 'January',
                        '02' => 'February',
                        '03' => 'March',
                        '04' => 'April',
                        '05' => 'May',
                        '06' => 'June',
                        '07' => 'July',
                        '08' => 'August',
                        '09' => 'September',
                        '10' => 'October',
                        '11' => 'November',
                        '12' => 'December'
                    );
        $y = $date->format('Y');
        return $date->format('j').' '.$sMonth[$date->format('m')].' '.$y;
    }

    public function convertFullDateThai($dDate) {

        $date = new DateTime($dDate);

        $sDayName = Array(
            'Sunday'        => 'อาทิตย์',
            'Monday'        => 'จันทร์',
            'Tuesday'       => 'อังคาร',
            'Wednesday'     => 'พุธ',
            'Thursday'      => 'พฤหัสบดี',
            'Friday'        => 'ศุกร์',
            'Saturday'      => 'เสาร์',
        );

        $sMonth = Array(
                        '01' => 'มกราคม',
                        '02' => 'กุมภาพันธ์',
                        '03' => 'มีนาคม',
                        '04' => 'เมษายน',
                        '05' => 'พฤษภาคม',
                        '06' => 'มิถุนายน',
                        '07' => 'กรกฏาคม',
                        '08' => 'สิงหาคม',
                        '09' => 'กันยายน',
                        '10' => 'ตุลาคม',
                        '11' => 'พฤศจิกายน',
                        '12' => 'ธันวาคม');

        $y = $date->format('Y') + 543;

        return $sDayName[$date->format('l')].' '.$date->format('j').' '.$sMonth[$date->format('m')].' '.$y;
    }

    public function dateDiff($date1, $date2) {
        $datetime1 = new DateTime($date1);
        $datetime2 = new DateTime($date2);
        $interval = $datetime1->diff($datetime2);
        return $interval->format('%d');
    }

    /*
    Generate Invoice Number
    */
    public function generateInvoice($oLastestInvoice) {

        if(empty($oLastestInvoice)) {

            $iInvoice = date("Ymd")."0001";
            return $iInvoice;

        } else {

            $sNumberInvoice = $oLastestInvoice->Invoice_Number;
            $sStripNumber   = substr($sNumberInvoice, 8, 6);

            // Strip Zero From Charactor And Plus Number
            $sInvoiceNumber = ltrim($sStripNumber,'0') + 1;

            $iInvoice       = sprintf("%04d",$sInvoiceNumber);

            return  date("Ymd").$iInvoice;

        }

    }
    /**
    * Convert Date With Send Language
    * @param string
    */
    public function convertDateWithSelectLang($dDate,$sLang) {

        switch ($sLang) {
            case 'th':
                $date = new DateTime($dDate);
                $sMonth = Array(
                                '01' => 'มกราคม',
                                '02' => 'กุมภาพันธ์',
                                '03' => 'มีนาคม',
                                '04' => 'เมษายน',
                                '05' => 'พฤษภาคม',
                                '06' => 'มิถุนายน',
                                '07' => 'กรกฏาคม',
                                '08' => 'สิงหาคม',
                                '09' => 'กันยายน',
                                '10' => 'ตุลาคม',
                                '11' => 'พฤศจิกายน',
                                '12' => 'ธันวาคม');
                $y = $date->format('Y') + 543;
                return $date->format('j').' '.$sMonth[$date->format('m')].' '.$y;
                break;
            case 'en':
                $date = new DateTime($dDate);
                $sMonth = Array(
                                '01' => 'January',
                                '02' => 'February',
                                '03' => 'March',
                                '04' => 'April',
                                '05' => 'May',
                                '06' => 'June',
                                '07' => 'July',
                                '08' => 'August',
                                '09' => 'September',
                                '10' => 'October',
                                '11' => 'November',
                                '12' => 'December'
                            );
                $y = $date->format('Y');
                return $date->format('j').' '.$sMonth[$date->format('m')].' '.$y;
                break;
            default:
                $date = new DateTime($dDate);
                $sMonth = Array(
                                '01' => 'มกราคม',
                                '02' => 'กุมภาพันธ์',
                                '03' => 'มีนาคม',
                                '04' => 'เมษายน',
                                '05' => 'พฤษภาคม',
                                '06' => 'มิถุนายน',
                                '07' => 'กรกฏาคม',
                                '08' => 'สิงหาคม',
                                '09' => 'กันยายน',
                                '10' => 'ตุลาคม',
                                '11' => 'พฤศจิกายน',
                                '12' => 'ธันวาคม');
                $y = $date->format('Y') + 543;
                return $date->format('j').' '.$sMonth[$date->format('m')].' '.$y;
                break;
        }

    }

    public function convertDigitMoney($iMoney) {
        return sprintf('%010d', $iMoney)."00";
    }

    public function generateDiscount($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function generateCode($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function convertDateDefault($sDate) {
        $date                           = $sDate;
        $convert                        = strtr($date, '/', '-');
        $sDateDefault                   = date('Y-m-d', strtotime($convert));

        return $sDateDefault;
    }

    public function convertDateForm($sDate) {
        $date                           = $sDate;
        $convert                        = strtr($date, '-', '/');
        $sDateDefault                   = date('d-m-Y', strtotime($convert));

        return $sDateDefault;
    }



    public function initConfigPagination($per_page,$total_rows,$base_url) {

        $this->CI->load->library('pagination');

        $config['base_url']        = base_url().$base_url."/";
        $config['per_page']        = $per_page;
        $config['total_rows']      = $total_rows;
        $config['full_tag_open']   = '<ul class="pagination">';
        $config['full_tag_close']  = '</ul>';
        $config['first_link']      = '&laquo; หน้าแรก';
        $config['first_tag_open']  = '<li class="prev page">';
        $config['first_tag_close'] = '</li>';
        $config['last_link']       = 'หน้าสุดท้าย &raquo;';
        $config['last_tag_open']   = '<li class="next page">';
        $config['last_tag_close']  = '</li>';
        $config['next_link']       = 'ถัดไป &rarr;';
        $config['next_tag_open']   = '<li class="next page">';
        $config['next_tag_close']  = '</li>';
        $config['prev_link']       = '&larr; ย้อนกลับ';
        $config['prev_tag_open']   = '<li class="prev page">';
        $config['prev_tag_close']  = '</li>';
        $config['cur_tag_open']    = '<li class="active"><a href='.base_url().$base_url.'>';
        $config['cur_tag_close']   = '</a></li>';
        $config['num_tag_open']    = '<li class="page">';
        $config['num_tag_close']   = '</li>';
        $config['anchor_class']    = 'follow_link';

        return $config;
        //return $this->CI->initialize($config);

    }

    public function tempPlace() {
        return $aDataTempPlace = array(
            array(
                "place_id" => 1,
                "place_th" => "ชลบุรี",
                "place_en" => "Chonburi"
            )
        );
    }


    public function tempCountry() {
       return $countries = array
            (
                'AF' => 'Afghanistan',
                'AX' => 'Aland Islands',
                'AL' => 'Albania',
                'DZ' => 'Algeria',
                'AS' => 'American Samoa',
                'AD' => 'Andorra',
                'AO' => 'Angola',
                'AI' => 'Anguilla',
                'AQ' => 'Antarctica',
                'AG' => 'Antigua And Barbuda',
                'AR' => 'Argentina',
                'AM' => 'Armenia',
                'AW' => 'Aruba',
                'AU' => 'Australia',
                'AT' => 'Austria',
                'AZ' => 'Azerbaijan',
                'BS' => 'Bahamas',
                'BH' => 'Bahrain',
                'BD' => 'Bangladesh',
                'BB' => 'Barbados',
                'BY' => 'Belarus',
                'BE' => 'Belgium',
                'BZ' => 'Belize',
                'BJ' => 'Benin',
                'BM' => 'Bermuda',
                'BT' => 'Bhutan',
                'BO' => 'Bolivia',
                'BA' => 'Bosnia And Herzegovina',
                'BW' => 'Botswana',
                'BV' => 'Bouvet Island',
                'BR' => 'Brazil',
                'IO' => 'British Indian Ocean Territory',
                'BN' => 'Brunei Darussalam',
                'BG' => 'Bulgaria',
                'BF' => 'Burkina Faso',
                'BI' => 'Burundi',
                'KH' => 'Cambodia',
                'CM' => 'Cameroon',
                'CA' => 'Canada',
                'CV' => 'Cape Verde',
                'KY' => 'Cayman Islands',
                'CF' => 'Central African Republic',
                'TD' => 'Chad',
                'CL' => 'Chile',
                'CN' => 'China',
                'CX' => 'Christmas Island',
                'CC' => 'Cocos (Keeling) Islands',
                'CO' => 'Colombia',
                'KM' => 'Comoros',
                'CG' => 'Congo',
                'CD' => 'Congo, Democratic Republic',
                'CK' => 'Cook Islands',
                'CR' => 'Costa Rica',
                'CI' => 'Cote D\'Ivoire',
                'HR' => 'Croatia',
                'CU' => 'Cuba',
                'CY' => 'Cyprus',
                'CZ' => 'Czech Republic',
                'DK' => 'Denmark',
                'DJ' => 'Djibouti',
                'DM' => 'Dominica',
                'DO' => 'Dominican Republic',
                'EC' => 'Ecuador',
                'EG' => 'Egypt',
                'SV' => 'El Salvador',
                'GQ' => 'Equatorial Guinea',
                'ER' => 'Eritrea',
                'EE' => 'Estonia',
                'ET' => 'Ethiopia',
                'FK' => 'Falkland Islands (Malvinas)',
                'FO' => 'Faroe Islands',
                'FJ' => 'Fiji',
                'FI' => 'Finland',
                'FR' => 'France',
                'GF' => 'French Guiana',
                'PF' => 'French Polynesia',
                'TF' => 'French Southern Territories',
                'GA' => 'Gabon',
                'GM' => 'Gambia',
                'GE' => 'Georgia',
                'DE' => 'Germany',
                'GH' => 'Ghana',
                'GI' => 'Gibraltar',
                'GR' => 'Greece',
                'GL' => 'Greenland',
                'GD' => 'Grenada',
                'GP' => 'Guadeloupe',
                'GU' => 'Guam',
                'GT' => 'Guatemala',
                'GG' => 'Guernsey',
                'GN' => 'Guinea',
                'GW' => 'Guinea-Bissau',
                'GY' => 'Guyana',
                'HT' => 'Haiti',
                'HM' => 'Heard Island & Mcdonald Islands',
                'VA' => 'Holy See (Vatican City State)',
                'HN' => 'Honduras',
                'HK' => 'Hong Kong',
                'HU' => 'Hungary',
                'IS' => 'Iceland',
                'IN' => 'India',
                'ID' => 'Indonesia',
                'IR' => 'Iran, Islamic Republic Of',
                'IQ' => 'Iraq',
                'IE' => 'Ireland',
                'IM' => 'Isle Of Man',
                'IL' => 'Israel',
                'IT' => 'Italy',
                'JM' => 'Jamaica',
                'JP' => 'Japan',
                'JE' => 'Jersey',
                'JO' => 'Jordan',
                'KZ' => 'Kazakhstan',
                'KE' => 'Kenya',
                'KI' => 'Kiribati',
                'KR' => 'Korea',
                'KW' => 'Kuwait',
                'KG' => 'Kyrgyzstan',
                'LA' => 'Lao People\'s Democratic Republic',
                'LV' => 'Latvia',
                'LB' => 'Lebanon',
                'LS' => 'Lesotho',
                'LR' => 'Liberia',
                'LY' => 'Libyan Arab Jamahiriya',
                'LI' => 'Liechtenstein',
                'LT' => 'Lithuania',
                'LU' => 'Luxembourg',
                'MO' => 'Macao',
                'MK' => 'Macedonia',
                'MG' => 'Madagascar',
                'MW' => 'Malawi',
                'MY' => 'Malaysia',
                'MV' => 'Maldives',
                'ML' => 'Mali',
                'MT' => 'Malta',
                'MH' => 'Marshall Islands',
                'MQ' => 'Martinique',
                'MR' => 'Mauritania',
                'MU' => 'Mauritius',
                'YT' => 'Mayotte',
                'MX' => 'Mexico',
                'FM' => 'Micronesia, Federated States Of',
                'MD' => 'Moldova',
                'MC' => 'Monaco',
                'MN' => 'Mongolia',
                'ME' => 'Montenegro',
                'MS' => 'Montserrat',
                'MA' => 'Morocco',
                'MZ' => 'Mozambique',
                'MM' => 'Myanmar',
                'NA' => 'Namibia',
                'NR' => 'Nauru',
                'NP' => 'Nepal',
                'NL' => 'Netherlands',
                'AN' => 'Netherlands Antilles',
                'NC' => 'New Caledonia',
                'NZ' => 'New Zealand',
                'NI' => 'Nicaragua',
                'NE' => 'Niger',
                'NG' => 'Nigeria',
                'NU' => 'Niue',
                'NF' => 'Norfolk Island',
                'MP' => 'Northern Mariana Islands',
                'NO' => 'Norway',
                'OM' => 'Oman',
                'PK' => 'Pakistan',
                'PW' => 'Palau',
                'PS' => 'Palestinian Territory, Occupied',
                'PA' => 'Panama',
                'PG' => 'Papua New Guinea',
                'PY' => 'Paraguay',
                'PE' => 'Peru',
                'PH' => 'Philippines',
                'PN' => 'Pitcairn',
                'PL' => 'Poland',
                'PT' => 'Portugal',
                'PR' => 'Puerto Rico',
                'QA' => 'Qatar',
                'RE' => 'Reunion',
                'RO' => 'Romania',
                'RU' => 'Russian Federation',
                'RW' => 'Rwanda',
                'BL' => 'Saint Barthelemy',
                'SH' => 'Saint Helena',
                'KN' => 'Saint Kitts And Nevis',
                'LC' => 'Saint Lucia',
                'MF' => 'Saint Martin',
                'PM' => 'Saint Pierre And Miquelon',
                'VC' => 'Saint Vincent And Grenadines',
                'WS' => 'Samoa',
                'SM' => 'San Marino',
                'ST' => 'Sao Tome And Principe',
                'SA' => 'Saudi Arabia',
                'SN' => 'Senegal',
                'RS' => 'Serbia',
                'SC' => 'Seychelles',
                'SL' => 'Sierra Leone',
                'SG' => 'Singapore',
                'SK' => 'Slovakia',
                'SI' => 'Slovenia',
                'SB' => 'Solomon Islands',
                'SO' => 'Somalia',
                'ZA' => 'South Africa',
                'GS' => 'South Georgia And Sandwich Isl.',
                'ES' => 'Spain',
                'LK' => 'Sri Lanka',
                'SD' => 'Sudan',
                'SR' => 'Suriname',
                'SJ' => 'Svalbard And Jan Mayen',
                'SZ' => 'Swaziland',
                'SE' => 'Sweden',
                'CH' => 'Switzerland',
                'SY' => 'Syrian Arab Republic',
                'TW' => 'Taiwan',
                'TJ' => 'Tajikistan',
                'TZ' => 'Tanzania',
                'TH' => 'Thailand',
                'TL' => 'Timor-Leste',
                'TG' => 'Togo',
                'TK' => 'Tokelau',
                'TO' => 'Tonga',
                'TT' => 'Trinidad And Tobago',
                'TN' => 'Tunisia',
                'TR' => 'Turkey',
                'TM' => 'Turkmenistan',
                'TC' => 'Turks And Caicos Islands',
                'TV' => 'Tuvalu',
                'UG' => 'Uganda',
                'UA' => 'Ukraine',
                'AE' => 'United Arab Emirates',
                'GB' => 'United Kingdom',
                'US' => 'United States',
                'UM' => 'United States Outlying Islands',
                'UY' => 'Uruguay',
                'UZ' => 'Uzbekistan',
                'VU' => 'Vanuatu',
                'VE' => 'Venezuela',
                'VN' => 'Viet Nam',
                'VG' => 'Virgin Islands, British',
                'VI' => 'Virgin Islands, U.S.',
                'WF' => 'Wallis And Futuna',
                'EH' => 'Western Sahara',
                'YE' => 'Yemen',
                'ZM' => 'Zambia',
                'ZW' => 'Zimbabwe',
            );
    }
}

?>
