<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masterdata {

    protected $CI;

    // We'll use a constructor, as you can't directly call a function
    // from a property definition.
    public function __construct() {
        // Assign the CodeIgniter super-object
        $this->CI =& get_instance();
    }

    public function menuData() {

       return $aTempMenu = array(
          "MN_CAR" => array(
                array(
                  "module_code"   => "MN_TYPE_CAR",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "ประเภทรถ",
                  "url"           => "manage_type_car",
                  "floor"         => 1
                ),
                array(
                  "module_code"   => "MN_BRAND_CAR",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "ตั้งค่าข้อมูลรถ",
                  "url"           => "manage_brand_car",
                  "floor"         => 1
                ),
                array(
                  "module_code"   => "MN_TYPE_MODEL_CAR",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "ตั้งค่าข้อมูลประเภทรุ่นรถ",
                  "url"           => "manage_type_model_car",
                  "floor"         => 1
                ),
                array(
                  "module_code"   => "MN_MODEL_CAR",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "ตั้งค่าข้อมูลรุ่นรถ",
                  "url"           => "manage_model_car",
                  "floor"         => 1
                ),
                array(
                  "module_code"   => "MN_FACILITY_CAR",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "สิ่งอำนวยความสะดวก",
                  "url"           => "manage_facility",
                  "floor"         => 1
                ),
                array(
                  "module_code"   => "MN_MAIN_OPTION_CAR",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "ออพชั่นหลัก",
                  "url"           => "manage_main_option",
                  "floor"         => 1
                ),
                array(
                  "module_code"   => "MN_SUB_OPTION_CAR",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "ออพชั่นเสริม",
                  "url"           => "manage_sub_option",
                  "floor"         => 1
                ),array(
                  "module_code"   => "MN_IMPORT_CAR_TO_BRANCH",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "นำรถเข้าสาขา",
                  "url"           => "import_car_to_branch",
                  "floor"         => 1
                ),
                array(
                  "module_code"   => "MN_WORKING_CAR",
                  "icon"          => "icon-puzzle",
                  "menu_name"     => "บริหารงานข้อมูลรถ",
                  "url"           => "manage_working_car",
                  "floor"         => 1
                ),
          ),
          "MN_SALE" => array(
              array(
                "module_code"     => "MN_SALE_BOOKING",
                "icon"            => "icon-puzzle",
                "menu_name"       => "ข้อมูลการจอง",
                "url"             => "manage_booking",
                "floor"           => 1
              ),
              array(
                "module_code"     => "MN_QUEUE_BOOKING",
                "icon"            => "icon-puzzle",
                "menu_name"       => "จัดคิวรถ",
                "url"             => "manage_queue_booking",
                "floor"           => 1
              ),
          ),
          "MN_PROMOTION" => array(
              array(
                "module_code"     => "MN_SETTING_PROMOTION",
                "icon"            => "icon-puzzle",
                "menu_name"       => "ตั้งค่า Promotion",
                "url"             => "manage_promotion",
                "floor"           => 1
              ),
              array(
                "module_code"     => "MN_SETTING_GIVE_PROMOTION",
                "icon"            => "icon-puzzle",
                "menu_name"       => "Promotion แบบแจก Code",
                "url"             => "manage_promotion_give",
                "floor"           => 1
              )
          ),
          "MN_REPORT" => array(
            array(
              "module_code"     => "MN_REPORT",
              "icon"            => "icon-puzzle",
              "menu_name"       => "รายงาน",
              "url"             => "manage_report/export_excel",
              "floor"           => 0
            )
        )
        );
    }

    public function fuelData() {
      return array(
        array(
          "fuel_id"   => 1,
          "fuel_name" => "น้ำมันเบนซิน 95",
        ),
        array(
          "fuel_id"   => 2,
          "fuel_name" => "น้ำมันโซฮอลล์ 95",
        ),
        array(
          "fuel_id"   => 3,
          "fuel_name" => "น้ำมันโซฮอลล์ 91",
        ),
        array(
          "fuel_id"   => 4,
          "fuel_name" => "น้ำมันโซฮอลล์ E20",
        ),
        array(
          "fuel_id"   => 5,
          "fuel_name" => "น้ำมัน E85",
        )

      );
    }

}

?>
