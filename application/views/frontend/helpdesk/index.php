<?php
$aUserdata = $this->session->userdata("userdata");
$rDept = $this->Home_model->getDeptByid($aUserdata["department_id"]);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Forms</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/theme.css" rel="stylesheet" media="all">

    <link href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
    <!-- <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all"> -->
    <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-register" style="min-height: 100vh;">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-10"  style="top: 70px !important; min-height: 84vh;padding-bottom: 100px;">
                              <div class="card">
                                    <div class="card-header text-center">
                                        <strong class="text-danger" style="font-size: 25px;padding-left: 84px;">Honda Lock</strong>
                                        <a href="<?php echo base_url("helpdesk/logout") ?>" class="btn btn-outline-danger btn-sm pull-right"><i class="fa fa-sign-out"></i>&nbsp;Log out</a>
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="<?php echo base_url("helpdesk/issue/save"); ?>" method="post" class="form-horizontal">
                                            <div class="row form-group">
                                              <div class="col col-md-12">
                                                <center>

                                                  <?php
                                                      if ($this->session->flashdata('msg-success')) {
                                                  ?>
                                                  <div class="alert alert-success alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-success'); ?>
                                                  </div>
                                                  <?php
                                                  } // end if msg
                                                  if ($this->session->flashdata('msg-danger')) {
                                                  ?>
                                                  <div class="alert alert-danger alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-danger'); ?>
                                                  </div>
                                                  <?php
                                                  } // end if msg
                                                  if ($this->session->flashdata('msg-update')) {
                                                  ?>
                                                  <div class="alert alert-info alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-update'); ?>
                                                  </div>
                                                  <?php
                                                  }// end if msg
                                                  ?>

                                                </center>
                                              </div>
                                            </div>


                                            <div class="row form-group">
                                              <div class="col col-md-12">
                                                <center>
                                                  <strong>รายการแจ้งซ่อม Hardware, Software และ Network ของคุณ</strong>
                                                </center>
                                              </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-12">
                                                  <a href="<?php echo base_url("helpdesk/issue/form"); ?>" class="btn btn-outline-primary btn-sm"><i class="fa fa-star"></i>&nbsp; New Issue</a>
                                                  <br><br>
                                                  <table id="example" class="display" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                          <th>Create Date</th>
                                                          <th>Issue No</th>
                                                          <th>Type of Request</th>
                                                          <th>Computer Name</th>
                                                          <th>Status</th>
                                                          <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                      <?php foreach ($aData as $kData => $vData) {?>
                                                              <tr>
                                                                  <td><?php echo $vData["createdate"]; ?></td>
                                                                  <td><?php echo sprintf("%05d", $vData["issue_id"]); ?></td>

                                                                  <td><?php $aType = unserialize($aData[0]["type_of_req"]) ?>
                                                                    <?php foreach ($aType as $kType => $vType) {
                                                                            echo $vType;
                                                                            if($kType < (count($aType)-1)){
                                                                              echo ",";
                                                                            }
                                                                          } ?>
                                                                  </td>
                                                                  <td><?php echo $vData["com_name"]; ?></td>
                                                                  <?php
                                                                      if($vData["status"] == 0){
                                                                        if(!empty($vData["approve_manager_by"])){
                                                                          echo '<td class="text-center"><span class="badge badge-success">Approve</span></td>';
                                                                        }else{
                                                                          echo '<td class="text-center"><span class="badge badge-secondary">Waiting</span></td>';
                                                                        }
                                                                        ?>
                                                                <?php }else if($vData["status"] == 1){?>
                                                                        <td class="text-center"><span class="badge badge-success">Success</span></td>
                                                                <?php }else if($vData["status"] == 2){?>
                                                                        <td class="text-center"><span class="badge badge-danger">Reject</span></td>
                                                                <?php }else if($vData["status"] == 3){?>
                                                                        <td class="text-center"><span class="badge badge-warning">In Progress</span></td>
                                                                <?php }else{echo "<td>-</td>";}?>
                                                                  <td>
                                                                    <a href="<?php echo base_url("helpdesk/issue/view?issue_id=".$vData["issue_id"]."") ?>" class="btn btn-outline-secondary"><i class="fa fa-eye"></i>&nbsp; View</a>
                                                                    <?php if($vData["status"] == 1 && $vData['feedback'] == null && $aUserdata['user_id'] == $vData['user_id']){ ?>
                                                                      <a href="#" class="btn btn-outline-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="<?php echo $vData["issue_id"] ?>"><i class="fas fa-comments"></i>&nbsp; Feedback</a>
                                                                    <?php } ?>
                                                                  </td>
                                                              </tr>
                                                      <?php } ?>
                                                      </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                          </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Feedback</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?php echo base_url("helpdesk/feedback"); ?>" method="post">
              <input type="hidden" name="issue_id" id="issue_id" value="">
              <div class="row">
                  <div class="col-md-12">
                      <!-- DATA TABLE-->
                      <div class="table-responsive">
                          <table class="table table-borderless table-data3">
                              <thead>
                                  <tr>
                                      <th>No.</th>
                                      <th>Topic</th>
                                      <th class="text-center"><i class="far fa-smile" style="font-size:50px"></i><br>Excellent</th>
                                      <th class="text-center"><i class="far fa-smile" style="font-size:50px"></i><br>Normal</th>
                                      <th class="text-center"><i class="far fa-smile" style="font-size:50px"></i><br>Bad</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td style="width:50px">1</td>
                                      <td>แก้ไขปัญหาได้ตรงตามการแจ้งซ่อม</td>
                                      <td class="text-center"><input type="radio" required name="feedback_level1" value="3" class="form-check-input"></td>
                                      <td class="text-center"><input type="radio" required name="feedback_level1" value="2" class="form-check-input"></td>
                                      <td class="text-center"><input type="radio" required name="feedback_level1" value="1" class="form-check-input"></td>
                                  </tr>
                                  <tr>
                                      <td style="width:50px">2</td>
                                      <td>ความรวดเร็วในการแก้ไขปัญหา (ไม่กระทบกับการทำงานของ User)</td>
                                      <td class="text-center"><input type="radio" required name="feedback_level2" value="3" class="form-check-input"></td>
                                      <td class="text-center"><input type="radio" required name="feedback_level2" value="2" class="form-check-input"></td>
                                      <td class="text-center"><input type="radio" required name="feedback_level2" value="1" class="form-check-input"></td>
                                  </tr>
                                  <tr>
                                      <td style="width:50px">3</td>
                                      <td>การแนะนำให้ความรู้เกี่ยวกับคอมพิวเตอร์ (ป้องกันการเกิดปัญหาซ้ำ)</td>
                                      <td class="text-center"><input type="radio" required name="feedback_level3" value="3" class="form-check-input"></td>
                                      <td class="text-center"><input type="radio" required name="feedback_level3" value="2" class="form-check-input"></td>
                                      <td class="text-center"><input type="radio" required name="feedback_level3" value="1" class="form-check-input"></td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                      <!-- END DATA TABLE-->
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Send</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </form>
        </div>
      </div>
    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>js/main.js"></script>
    <script src="<?php echo base_url("helpdesk/assets/lib/bootstrap-chosen-master/chosen-jquery.js")?>"></script>
    <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        $('.select-chosen').chosen();
        $('#example').DataTable({
            "order": [[ 0, "desc" ]]
        });

        $('select').css('border', '1px solid gainsboro');
        $('select').css('box-shadow', '0 0 1px rgb(144, 144, 144)');
        $('input').css('border', '1px solid gainsboro');
        $('input').css('box-shadow', '0 0 1px rgb(144, 144, 144)');

        $('#exampleModal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var recipient = button.data('whatever') // Extract info from data-* attributes
          // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
          // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
          var modal = $(this)
          modal.find('.modal-body input#issue_id').val(recipient)
        })

      });
    </script>
</body>

</html>
<!-- end document-->
