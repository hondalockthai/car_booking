<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Forms</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/theme.css" rel="stylesheet" media="all">

    <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-register" style="height: 100vh;">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-8"  style="top: 70px !important; height: 84vh;">
                              <div class="card">
                                    <div class="card-header">
                                        <strong>Register</strong> Form <span class="pull-right"><a class="btn btn-outline-secondary btn-sm" href="<?php echo base_url("helpdesk/login"); ?>">Back</a></span>
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="<?php echo base_url("helpdesk/register/save"); ?>" method="post" class="form-horizontal">
                                            <div class="row form-group">
                                              <div class="col col-md-12">
                                                <center>

                                                  <?php
                                                      if ($this->session->flashdata('msg-success')) {
                                                  ?>
                                                  <div class="alert alert-success alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-success'); ?>
                                                  </div>
                                                  <?php
                                                  } // end if msg
                                                  if ($this->session->flashdata('msg-danger')) {
                                                  ?>
                                                  <div class="alert alert-danger alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-danger'); ?>
                                                  </div>
                                                  <?php
                                                  } // end if msg
                                                  if ($this->session->flashdata('msg-update')) {
                                                  ?>
                                                  <div class="alert alert-info alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-update'); ?>
                                                  </div>
                                                  <?php
                                                  }// end if msg
                                                  ?>

                                                </center>
                                              </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sUsername" class=" form-control-label">Username :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="sUsername" class="form-control" maxlength="50" required>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sPassword" class="form-control-label">Password :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="password" name="sPassword" class="form-control" maxlength="50" required>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sFirstname" class=" form-control-label">Firstname :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="sFirstname" class="form-control" maxlength="50" required>
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sLastname" class=" form-control-label">Lastname :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="sLastname" class="form-control" maxlength="50" required>
                                                </div>
                                            </div>
                                            <!-- <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sCompName" class=" form-control-label">Computer Name :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="sCompName" class="form-control"  maxlength="30" required>
                                                </div>
                                            </div> -->
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="iDept" class=" form-control-label">Dept :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <select name="iDept" class="form-control select-chosen" required>
                                                      <option value="">Please choose</option>
                                                      <?php foreach ($aDept as $kDept => $vDept): ?>
                                                          <option value="<?php echo $vDept["department_id"] ?>"><?php echo $vDept["department_name"] ?></option>
                                                      <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sTel" class=" form-control-label">Tel :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="sTel" class="form-control" maxlength="15" required>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="card-footer">
                                      <center>
                                        <button type="submit" class="btn btn-primary btn-sm">
                                          <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                          <i class="fa fa-ban"></i> Reset
                                        </button>
                                      </center>
                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>js/main.js"></script>
    <script src="<?php echo base_url("helpdesk/assets/lib/bootstrap-chosen-master/chosen-jquery.js")?>"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        $('.select-chosen').chosen();
      });
    </script>
</body>

</html>
<!-- end document-->
