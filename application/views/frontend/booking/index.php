<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Booking Car Center</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="<?php echo base_url("assets/template/dist/css/site.min.css") ?>">
    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css"> -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo base_url("assets/template/dist/js/site.min.js") ?>"></script>
  </head>
  <body style="font-size: 16px;">
    <!--nav-->
    <nav role="navigation" class="navbar navbar-custom">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Booking Car Center</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <!-- <li class="active"><a href="getting-started.html"></a></li>
              <li class="active"><a href="index.html">ออกจากระบบ</a></li> -->
              <!-- <li class="disabled"><a href="#">Link</a></li> -->
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $aUserData["memb_username"] ?> <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <!-- <li class="dropdown-header">Setting</li> -->
                  <!-- <li><a href="#">Action</a></li> -->
                  <!-- <li class="divider"></li> -->
                  <!-- <li class="active"><a href="#">Separated link</a></li> -->
                  <!-- <li class="divider"></li> -->
                  <li class="disabled"><a href="<?php echo base_url("logout"); ?>">ออกจากระบบ</a></li>
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    <!--header-->
    <div class="container-fluid">
    <!--documents-->
        <div class="row row-offcanvas row-offcanvas-left">
          <div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
            <ul class="list-group panel">
                <li class="list-group-item"><i class="glyphicon glyphicon-align-justify"></i> <b>เมนู</b></li>
                <li class="list-group-item">ฟหกฟหก</li>
                <li class="list-group-item active"><a href="<?php echo base_url("home"); ?>" style="color:white"><i class="glyphicon glyphicon-home"></i>หน้าหลัก </a></li>
                <li class="list-group-item"><a href="<?php echo base_url("reservation/step1"); ?>"><i class="fa fa-bus"></i>จองรถรับส่ง </a></li>
                <li class="list-group-item"><a href="<?php echo base_url("booking"); ?>"><i class="glyphicon glyphicon-calendar"></i>ตรวจสอบรายชื่อผู้จองรถรับ-ส่ง </a></li>
                <li class="list-group-item"><a href="<?php echo base_url("member"); ?>"><i class="fa fa-users"></i>รายชื่อพนักงาน </a></li>
              </ul>
          </div>
          <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar"><span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a> หน้าหลัก</h3>
              </div>
              <div class="panel-body">

                  <div class="content-row">
                    <h2 class="content-row-title">ระบบจองรถรับส่งพนักงาน</h2>
                    <div class="row">


                      <div class="col-md-8">
                        <div class="jumbotron">
                          <div id="carousel-content-row-generic" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-content-row-generic" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-content-row-generic" data-slide-to="1"></li>
                              <li data-target="#carousel-content-row-generic" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                              <div class="item active">
                                <img src="http://localhost/car_booking/assets/template/dist/img/slider1.jpg">
                              </div>
                              <div class="item">
                                <img src="http://localhost/car_booking/assets/template/dist/img/slider2.jpg">
                              </div>
                              <div class="item">
                                <img src="http://localhost/car_booking/assets/template/dist/img/slider3.jpg">
                              </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-content-row-generic" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-content-row-generic" data-slide="next">
                              <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                          </div>
                          <div class="jumbotron-contents">
                            <h1>Implementing the HTML and CSS into your user interface project</h1>
                            <h2>HTML Structure</h2>
                            <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                            <h2>CSS Structure</h2>
                            <p>This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4">
                      <?php
                        $cntPeople = 1228;
                        $cnt5 = 300;
                        $cnt4 = 300;
                        $cnt3 = 14;
                        $cnt2 = 57;
                        $cnt1 = $cntPeople-($cnt2+$cnt3+$cnt4+$cnt5);
                      ?>
                      <div class="list-group">
                        <a href="#" class="list-group-item active">ประจำวันที่ <?php echo date("d/m/Y"); ?></a>
                        <a href="#" class="list-group-item"><span class="badge badge-primary"><?php echo $cnt1;?></span>08.00 - 17.00</a>
                        <a href="#" class="list-group-item"><span class="badge badge-primary"><?php echo $cnt2;?></span>17.20 - 20.20</a>
                        <a href="#" class="list-group-item"><span class="badge badge-primary"><?php echo $cnt3;?></span>17.20 - 23.20</a>
                        <a href="#" class="list-group-item"><span class="badge badge-primary"><?php echo $cnt4;?></span>16.40 - 00.50</a>
                        <a href="#" class="list-group-item"><span class="badge badge-primary"><?php echo $cnt5;?></span>00.40 - 7.10</a>
                    </div>
                      </div>
                </div>
                </div>
              </div><!-- panel body -->
            </div>
        </div><!-- content -->
      </div>
    </div>
    <!--footer-->
  </body>
</html>
