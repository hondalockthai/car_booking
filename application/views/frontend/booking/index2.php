<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Booking Car Center</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="<?php echo base_url("assets/template/dist/css/site.min.css") ?>">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">

    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css"> -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo base_url("assets/template/dist/js/site.min.js") ?>"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </head>
  <body style="font-size: 16px;">
    <input type="hidden" id="base_url" value="<?php echo base_url(""); ?>">
    <!--nav-->
    <nav role="navigation" class="navbar navbar-custom">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Booking Car Center</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <!-- <li class="active"><a href="getting-started.html"></a></li>
              <li class="active"><a href="index.html">ออกจากระบบ</a></li> -->
              <!-- <li class="disabled"><a href="#">Link</a></li> -->
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $aUserData["memb_username"] ?> <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <!-- <li class="dropdown-header">Setting</li> -->
                  <!-- <li><a href="#">Action</a></li> -->
                  <!-- <li class="divider"></li> -->
                  <!-- <li class="active"><a href="#">Separated link</a></li> -->
                  <!-- <li class="divider"></li> -->
                  <li class="disabled"><a href="<?php echo base_url("logout"); ?>">ออกจากระบบ</a></li>
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    <!--header-->
    <div class="container-fluid">
    <!--documents-->
        <div class="row row-offcanvas row-offcanvas-left">
          <div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
            <ul class="list-group panel">
                <li class="list-group-item"><i class="glyphicon glyphicon-align-justify"></i> <b>เมนู</b></li>
                <li class="list-group-item">ฟหกฟหก</li>
                <li class="list-group-item"><a href="<?php echo base_url("home"); ?>"><i class="glyphicon glyphicon-home"></i>หน้าหลัก </a></li>
                <li class="list-group-item"><a href="<?php echo base_url("reservation/step1"); ?>"><i class="fa fa-bus"></i>จองรถรับส่ง </a></li>
                <li class="list-group-item active"><a href="<?php echo base_url("booking"); ?>" style="color:white"><i class="glyphicon glyphicon-calendar"></i>ตรวจสอบรายชื่อผู้จองรถรับ-ส่ง </a></li>
                <li class="list-group-item"><a href="<?php echo base_url("member"); ?>"><i class="fa fa-users"></i>รายชื่อพนักงาน </a></li>
              </ul>
          </div>
          <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar"><span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a> หน้าหลัก</h3>
              </div>
              <div class="panel-body">

                  <div class="content-row">
                    <h2 class="content-row-title">ระบบจองรถรับส่งพนักงาน</h2>
                    <div class="row">

                      <div class="content-row">

                  <div class="panel panel-primary">
                    <div class="panel-heading">
                      <div class="panel-title"><b>ตรวจสอบรายชื่อผู้จองรถรับ-ส่ง</b>
                      </div>

                      <div class="panel-options">
                        <a class="bg" data-target="#sample-modal-dialog-1" data-toggle="modal" href="#sample-modal"><i class="entypo-cog"></i></a>
                        <a data-rel="collapse" href="#"><i class="entypo-down-open"></i></a>
                        <a data-rel="close" href="#!/tasks" ui-sref="Tasks"><i class="entypo-cancel"></i></a>
                      </div>
                    </div>

                    <div class="panel-body">
                      <p class="text-center"><img src="<?php echo base_url("assets/image/booking.jpg") ?>" width="200px"></p>
                      <form method="post" role="form" class="form-horizontal" action="<?php echo base_url("reservation/step2?reserv_id=".date("YmdHis").""); ?>">
                        <div class="form-group">
                          <label class="col-md-5 control-label">วันที่ต้องการจอง</label>
                          <div class="col-md-2">
                            <input type="text" id="datepicker" name="dReserv"  autocomplete="off" required="" class="form-control">
                          </div>

                        </div>
                        <div class="form-group">
                          <label class="col-md-5 control-label">ช่วงเวลา</label>
                          <div class="col-md-4">
                            <select type="date" name="tReserv" required="" class="form-control">
                              <option value="">กรุณาเลือกช่วงเวลา</option>
                              <?php foreach ($aTime as $key => $value) {?>
                                      <option value="<?php echo $value["id"]; ?>"><?php echo $value["time_name"]; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-offset-5 col-md-10">
                            <button class="btn btn-info" id="btnSearch" style="width:150px" type="button">ค้นหา</button>
                          </div>
                        </div>
                      </form>

                      <div id="searchTbl" style="display:block;">

                      <hr>
                      <table id="tblList" class="display table table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th class="text-center" width="80px">ลำดับ</th>
                                <th class="text-center" width="160px">รหัสพนักงาน</th>
                                <th class="text-center">ชื่อ - นามสกุล</th>
                                <th class="text-center" width="120px">ต้องการรถ</th>
                                <th class="text-center" width="180px">ต้องการอาหาร</th>
                                <th class="text-center">สายรถ</th>
                                <th class="text-center">สายรถ</th>
                            </tr>
                        </thead>
                        <tbody>

                              <tr>
                                  <td class="text-center" width="80px">ลำดับ</td>
                                  <td class="text-center" width="160px">รหัสพนักงาน</td>
                                  <td class="text-center">ชื่อ - นามสกุล</td>
                                  <td class="text-center" width="120px">ต้องการรถ</td>
                                  <td class="text-center" width="180px">ต้องการอาหาร</td>
                                  <td class="text-center">สายรถ</td>
                                  <td class="text-center">สายรถ</td>
                              </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                      </div>
                    </div>
                </div>
              </div><!-- panel body -->
            </div>
        </div><!-- content -->
      </div>
    </div>
    <!--footer-->
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="border-color: #DA4453;background-color: #DA4453;color: white;">
                <h4>ยกเลิกการจองรถ</h4>
            </div>
            <div class="modal-body">
              <br>
                คุณต้องการยกเลิกการจองรายการนี้จริงหรือไม่?
            </div>
            <hr>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                <a class="btn btn-danger btn-ok">ยืนยัน</a>
            </div>
        </div>
    </div>
</div>
  </body>
  <script type="text/javascript">
    $(document).ready(function() {

             var dateToday = new Date();
             $('#datepicker').datepicker({
               numberOfMonths: 1,
               // minDate: dateToday,
               dateFormat: 'dd/mm/yy'
             }).datepicker("setDate", new Date());
    });
  </script>
  <!-- <script src="https://code.jquery.com/jquery-3.5.1.js" charset="utf-8"></script> -->
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" charset="utf-8"></script>

  <script type="text/javascript">
  var table;

    var base_url = $('#base_url').val();
    $(document).ready(function() {
      var sDatepicker = $('#datepicker').val();
      // var ressDatepicker = sDatepicker.replace("-", "/");
      table = $('#tblList').DataTable( {"pageLength": 25,
          // "iDisplayLength" : 5, // Limit Per Page
          "sAjaxSource": base_url+"booking/jsonviewdata/"+sDatepicker,
          "aoColumns": [  // Setup Column Title
              {
                  "sTitle": "ลำดับ",
                  "sClass" : "text-center",
                  "width" : "5%"
              },
              {
                  "sTitle": "รหัสพนักงาน",
                  "sClass" : "text-center",
                  "width" : "10%"
              },
  			      {
                  "sTitle": "ชื่อ - นามสกุล",
              },
              {
                  "sTitle": "รถรับส่ง",
                  "sClass" : "text-center",
                  "width" : "10%"
              },
              {
                  "sTitle": "อาหาร",
                  "sClass" : "text-center",
                  "width" : "10%"
              },
              {
                  "sTitle": "สายรถ",
              },
              {
                  "sTitle": "จัดการ",
                  "sClass" : "text-center",
                  "width" : "10%"
              },
          ],
          columnDefs: [
              { orderable: false, targets: 6 }
              // { orderable: false, targets: 2 }
          ],
          "oLanguage": {
                      "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                      "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                      "sInfoEmpty":      "แสดง 0 ถึง 0 จาก 0 แถว",
                      "sInfoFiltered":   "(กรองข้อมูล _MAX_ ทุกแถว)",
                      "sInfoPostFix":    "",
                      "sInfoThousands":  ",",
                      "sLengthMenu":     "แสดง _MENU_ แถว",
                      "sLoadingRecords": "กำลังโหลดข้อมูล...",
                      "sProcessing":     "กำลังดำเนินการ...",
                      "sSearch":         "ค้นหา: ",
                      "sZeroRecords":    "ไม่พบข้อมูล",
                      "oPaginate": {
                          "sFirst":    "หน้าแรก",
                      "sPrevious": "ก่อนหน้า",
                          "sNext":     "ถัดไป",
                      "sLast":     "หน้าสุดท้าย"
                      },
                      "oAria": {
                          "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                      "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                      }
            }
       } );





      $('body').on('click','#btnSearch',function(){
        var sDatepicker = $('#datepicker').val();
        table.ajax.url(base_url+"booking/jsonviewdata/"+sDatepicker ).load();


        $('#searchTbl').show();
      });
      // var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
    } );

    function myFunction(id){
      if(confirm('คุณต้องการยกเลิกการจองรายการนี้จริงหรือไม่?')){
        $.ajax({
          url: base_url+'reservation/cancel',
          type: 'POST',
          dataType: 'json',
          data: {id: id}
        })
        .done(function(res) {
          console.log(res);
          var sDatepicker = $('#datepicker').val();
          table.ajax.url(base_url+"booking/jsonviewdata/"+sDatepicker ).load();
        })

      }
    }
  </script>
</html>
