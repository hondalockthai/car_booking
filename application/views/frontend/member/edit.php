<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Booking Car Center</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="<?php echo base_url("assets/template/dist/css/site.min.css") ?>">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
    <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css"> -->
    <!-- <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'> -->
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php echo base_url("assets/template/dist/js/site.min.js") ?>"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://jqueryui.com/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <style media="screen">
    table.dataTable thead th, table.dataTable thead td {
  padding: 10px 18px;
  border-bottom: 0px solid #111 !important;
}
table.dataTable.no-footer {
    border-bottom: 0px solid #111;
}
input[type=search] {
    -webkit-appearance: auto;
}
    </style>
  </head>
  <body style="font-size: 16px;">
    <!--nav-->
    <nav role="navigation" class="navbar navbar-custom">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button data-target="#bs-content-row-navbar-collapse-5" data-toggle="collapse" class="navbar-toggle" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand">Booking Car Center</a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div id="bs-content-row-navbar-collapse-5" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
              <!-- <li class="active"><a href="getting-started.html"></a></li>
              <li class="active"><a href="index.html">ออกจากระบบ</a></li> -->
              <!-- <li class="disabled"><a href="#">Link</a></li> -->
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $aUserData["memb_username"] ?> <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu">
                  <!-- <li class="dropdown-header">Setting</li> -->
                  <!-- <li><a href="#">Action</a></li> -->
                  <!-- <li class="divider"></li> -->
                  <!-- <li class="active"><a href="#">Separated link</a></li> -->
                  <!-- <li class="divider"></li> -->
                  <li class="disabled"><a href="<?php echo base_url("logout"); ?>">ออกจากระบบ</a></li>
                </ul>
              </li>
            </ul>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    <!--header-->
    <div class="container-fluid">
    <!--documents-->
        <div class="row row-offcanvas row-offcanvas-left">
          <div class="col-xs-6 col-sm-3 sidebar-offcanvas" role="navigation">
            <ul class="list-group panel">
                <li class="list-group-item"><i class="glyphicon glyphicon-align-justify"></i> <b>เมนู</b></li>
                <li class="list-group-item">ฟหกฟหก</li>
                <li class="list-group-item"><a href="<?php echo base_url("home"); ?>"><i class="glyphicon glyphicon-home"></i>หน้าหลัก </a></li>
                <li class="list-group-item"><a href="<?php echo base_url("reservation/step1"); ?>"><i class="fa fa-bus"></i>จองรถรับส่ง </a></li>
                <li class="list-group-item"><a href="<?php echo base_url("booking"); ?>"><i class="glyphicon glyphicon-calendar"></i>ตรวจสอบรายชื่อผู้จองรถรับ-ส่ง </a></li>
                <li class="list-group-item active"><a href="<?php echo base_url("member"); ?>" style="color:white"><i class="fa fa-users"></i>รายชื่อพนักงาน </a></li>
              </ul>
          </div>
          <div class="col-xs-12 col-sm-9 content">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title"><a href="javascript:void(0);" class="toggle-sidebar"><span class="fa fa-angle-double-left" data-toggle="offcanvas" title="Maximize Panel"></span></a> หน้าหลัก</h3>
              </div>
              <div class="panel-body">

                  <div class="content-row">
                    <h2 class="content-row-title">ระบบจองรถรับส่งพนักงาน</h2>
                    <div class="row">
                      <div class="content-row">

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <div class="panel-title"><b>ข้อมูลพนักงาน</b>
                      </div>

                      <div class="panel-options">
                        <a class="bg" data-target="#sample-modal-dialog-1" data-toggle="modal" href="#sample-modal"><i class="entypo-cog"></i></a>
                        <a data-rel="collapse" href="#"><i class="entypo-down-open"></i></a>
                        <a data-rel="close" href="#!/tasks" ui-sref="Tasks"><i class="entypo-cancel"></i></a>
                      </div>
                    </div>

                    <div class="panel-body">
                      <form novalidate="" role="form" class="form-horizontal">
                        <div class="form-group">
                          <label class="col-md-2 control-label">ชื่อ</label>
                          <div class="col-md-10">
                            <input type="text" required="" placeholder="Title" id="title" class="form-control" name="title">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-2 control-label">Subject</label>
                          <div class="col-md-10">
                            <input type="text" required="" placeholder="Subject" id="subject" class="form-control" name="title">
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="col-md-2 control-label" for="description">Description</label>
                          <div class="col-md-10">
                            <textarea required="" class="form-control" placeholder="Description" rows="10" cols="30" id="description" name="description"></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-offset-2 col-md-10">
                            <button class="btn btn-info" type="submit">Submit</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
                    </div>
                </div>
              </div><!-- panel body -->
            </div>
        </div><!-- content -->
      </div>
    </div>

    <!--footer-->
  </body>

  <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">New message</h4>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group">
              <label for="recipient-name" class="control-label">Recipient:</label>
              <input type="text" class="form-control" id="recipient-name">
            </div>
            <div class="form-group">
              <label for="message-text" class="control-label">Message:</label>
              <textarea class="form-control" id="message-text"></textarea>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Send message</button>
        </div>
      </div>
    </div>
  </div> -->

  <script src="https://code.jquery.com/jquery-3.5.1.js" charset="utf-8"></script>
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).ready(function() {

      // $('body').on('click','.idprimary',function(){
      //   alert($(this).data("whatever"));
      // })
      // $('body').on('click','.changeBuspoint',function(){
      //   alert($(this).data("emp_no"));
      // })

      $('#exampleModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('New message to ' + recipient)
        modal.find('.modal-body input').val(recipient)
      })

      $('#example').DataTable({
          "oLanguage": {
                      "sEmptyTable":     "ไม่มีข้อมูลในตาราง",
                      "sInfo":           "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                      "sInfoEmpty":      "แสดง 0 ถึง 0 จาก 0 แถว",
                      "sInfoFiltered":   "(กรองข้อมูล _MAX_ ทุกแถว)",
                      "sInfoPostFix":    "",
                      "sInfoThousands":  ",",
                      "sLengthMenu":     "แสดง _MENU_ แถว",
                      "sLoadingRecords": "กำลังโหลดข้อมูล...",
                      "sProcessing":     "กำลังดำเนินการ...",
                      "sSearch":         "ค้นหา: ",
                      "sZeroRecords":    "ไม่พบข้อมูล",
                      "oPaginate": {
                          "sFirst":    "หน้าแรก",
                      "sPrevious": "ก่อนหน้า",
                          "sNext":     "ถัดไป",
                      "sLast":     "หน้าสุดท้าย"
                      },
                      "oAria": {
                          "sSortAscending":  ": เปิดใช้งานการเรียงข้อมูลจากน้อยไปมาก",
                      "sSortDescending": ": เปิดใช้งานการเรียงข้อมูลจากมากไปน้อย"
                      }
            }
      });



      // var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();
    } );
  </script>
</html>
