<?php
$aUserAdmin = $this->session->userdata('userdata_admin');
$arrMonth = array("January","February","March","April","May","June","July","August","September","October","November","December");
$valueMonth = $this->input->get('month');
$aData  = $this->Report_model->getDataCondd((array_search($valueMonth, $arrMonth)+1),'All');
$Hardware     = 0;
$Software     = 0;
$Network      = 0;
$Other        = 0;
$aDept        = array();
foreach ($aData as $kData => $vData) {
  $dataUnserial = unserialize($vData['type_of_req']);
  if(in_array("Hardware",$dataUnserial)){
    $Hardware++;
  }
  if(in_array("Software",$dataUnserial)){
    $Software++;
  }
  if(in_array("Network",$dataUnserial)){
    $Network++;
  }
  if(in_array("Other",$dataUnserial)){
    $Other++;
  }
  array_push($aDept,$vData["dept_name"]);
}
$aDataCnt = array(
  'Hardware'  => $Hardware,
  'Software'  => $Software,
  'Network'   => $Network,
  'Other'     => $Other,
);
// echo "<pre>";
// print_r($aDept);

$aDeptGroup = array_unique($aDept);
// print_r($aDept);
foreach ($aDeptGroup as $kDeptGroup => $vDeptGroup) {
  $cnt = 0;
  foreach ($aDept as $kDept => $vDept) {
    if($vDept == $vDeptGroup){
      $cnt++;
    }
  }
  $aSortDeptIssue[$kDeptGroup] = ''.$cnt.'#'.$vDeptGroup.'';
  // $aSortDeptIssue[$kDeptGroup] = array(
  //   'dept_name' => $vDeptGroup,
  //   'cnt_issue' => $cnt,
  // );
}
rsort($aSortDeptIssue);
// print_r($aSortDeptIssue);
// die();
$arrayType = array($aDataCnt["Hardware"],$aDataCnt["Software"],$aDataCnt["Network"],$aDataCnt["Other"]);
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>SysAdmin</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/font-face.css" rel="stylesheet" media="all">
    <!-- <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all"> -->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/theme.css" rel="stylesheet" media="all">

    <link href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
    <!-- <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all"> -->
    <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all">
    <style media="screen">
        body {
          font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        #chartdiv {
          width: 100%;
          min-height: 75vh;
        }

    </style>
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
</head>

<body class="animsition">
  <input type="hidden" id="base_url" value="<?php echo base_url("") ?>">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="#">
                            <strong class="text-danger" style="font-size:30px">HONDA LOCK</strong>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">

                            <li>
                                <a href="<?php echo base_url("helpdesk/admin_issue/list") ?>">
                                    <i class="fas fa-list-ol"></i>
                                    <span class="bot-line"></span>Issue List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url("helpdesk/admin_issue/report?year=".date('Y')."") ?>">
                                    <i class="fas fa-chart-bar"></i>
                                    <span class="bot-line"></span>Report</a>
                            </li>
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-cogs"></i>
                                    <span class="bot-line"></span>Option</a>
                                <ul class="header3-sub-list list-unstyled">
                                  <li>
                                    <a href="<?php echo base_url("helpdesk/admin_issue/member"); ?>"><i class="fas fa-users"></i> Member</a>
                                  </li>
                                  <!-- <li>
                                    <a href="button.html"><i class="fas fa-cogs"></i> Setting E-mail</a>
                                  </li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="header-button-item">
                          <a href="<?php echo base_url("helpdesk/admin_logout") ?>">
                            <i class="fas fa-sign-out-alt"style="font-size: 20px;"></i> <span style="font-size:20px;"> Logout</span>
                          </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">

            <!-- STATISTIC CHART-->
            <section class="statistic-chart">
                <div class="container card">
                    <div class="row">
                        <div class="col-md-12">
                          <br>
                            <h3 class="text-center">Report issues : <u><?php echo $this->input->get('month').' '.$this->input->get('year'); ?></u></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                          <div id="chartdiv"></div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                          <div id="divLoad">
                            <div class="container">
                              <h3 class="text-center">Report Detail</h3>
                              <br>
                              <table class="table table-bordered table-striped table-hover" id="tblReportDetail">
                                <thead class="thead-dark">
                                  <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">Department</th>
                                    <th class="text-center">Issue Qty</th>
                                    <th class="text-center">Success</th>
                                    <th class="text-center">In Process</th>
                                    <th class="text-center">Reject</th>
                                    <th class="text-center">Waiting</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $issueAll       = 0;
                                  foreach ($aSortDeptIssue as $key => $vDept) {
                                    $issueSuccess   = 0;
                                    $issueInProcess = 0;
                                    $issueReject    = 0;
                                    $issueWaiting   = 0;
                                    $aDataDept    = explode('#',$vDept);
                                    $cntSuccess   = $this->Report_model->getCntByDept((array_search($valueMonth, $arrMonth)+1),'1',$aDataDept[1]);
                                    $cntInProcess = $this->Report_model->getCntByDept((array_search($valueMonth, $arrMonth)+1),'3',$aDataDept[1]);
                                    $cntReject    = $this->Report_model->getCntByDept((array_search($valueMonth, $arrMonth)+1),'2',$aDataDept[1]);
                                    $cntWaiting   = $this->Report_model->getCntByDept((array_search($valueMonth, $arrMonth)+1),'0',$aDataDept[1]);
                                    ?>
                                      <tr>
                                        <td class="text-center"><?php echo ($key+1) ?></td>
                                        <td><?php echo $aDataDept[1] ?></td>
                                        <td class="text-right"><?php echo $aDataDept[0] ?></td>
                                        <td class="text-right"><?php echo (!empty($cntSuccess))?$cntSuccess['cnt']:0; ?></td>
                                        <td class="text-right"><?php echo (!empty($cntInProcess))?$cntInProcess['cnt']:0; ?></td>
                                        <td class="text-right"><?php echo (!empty($cntReject))?$cntReject['cnt']:0; ?></td>
                                        <td class="text-right"><?php echo (!empty($cntWaiting))?$cntWaiting['cnt']:0; ?></td>
                                      </tr>
                                  <?php
                                    $issueAll += $aDataDept[0];
                                    $issueSuccess += (!empty($cntSuccess))?$cntSuccess['cnt']:0;;
                                    $issueInProcess += (!empty($cntInProcess))?$cntInProcess['cnt']:0;;
                                    $issueReject += (!empty($cntReject))?$cntReject['cnt']:0;;
                                    $issueWaiting += (!empty($cntWaiting))?$cntWaiting['cnt']:0;;
                                  } ?>
                                </tbody>
                                <tfoot>
                                  <tr>
                                    <td colspan="2"><strong>Total</strong></td>
                                    <td class="text-right"><strong><?php echo $issueAll ?></strong></td>
                                    <td class="text-right"><strong><?php echo $issueSuccess ?></strong></td>
                                    <td class="text-right"><strong><?php echo $issueInProcess ?></strong></td>
                                    <td class="text-right"><strong><?php echo $issueReject ?></strong></td>
                                    <td class="text-right"><strong><?php echo $issueWaiting ?></strong></td>
                                  </tr>
                                </tfoot>
                              </table>
                              <br>
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-lg-12 text-center">
                          <a href="<?php echo $this->agent->referrer(); ?>" class="btn btn-danger btn-sm">
                            <i class="fa fa-chevron-left"></i> Back
                          </a>
                        </div>
                    </div>
                    <br>

                </div>
            </section>
            <!-- END STATISTIC CHART-->

            <!-- COPYRIGHT-->
            <section class="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.js"></script>

    <!-- Main JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>js/main.js"></script>
    <script src="<?php echo base_url("helpdesk/assets/lib/bootstrap-chosen-master/chosen-jquery.js")?>"></script>
    <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        var base_url = $('#base_url').val();
        var loadKeyWord = '';
        /**
         * ---------------------------------------
         * This demo was created using amCharts 4.
         *
         * For more information visit:
         * https://www.amcharts.com/
         *
         * Documentation is available at:
         * https://www.amcharts.com/docs/v4/
         * ---------------------------------------
         */

        // Themes begin
        am4core.useTheme(am4themes_animated);
        // Themes end

        var chart = am4core.create("chartdiv", am4charts.PieChart3D);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.legend = new am4charts.Legend();

        chart.data = [
          {
            country: "Hardware",
            litres: <?php echo $arrayType[0] ?>
          },
          {
            country: "Software",
            litres: <?php echo $arrayType[1] ?>
          },
          {
            country: "Network",
            litres: <?php echo $arrayType[2] ?>
          },
          {
            country: "Other",
            litres: <?php echo $arrayType[3] ?>
          }
        ];

        var pieSeries = chart.series.push(new am4charts.PieSeries3D());
        pieSeries.dataFields.value = "litres";
        pieSeries.dataFields.category = "country";
        chart.legend.position = "right";
        pieSeries.colors.step = 4;

        // pieSeries.slices.template.events.on("hit", function(ev) {
        //   var valCate = ev.target.dataItem.category;
        //
        //   $('#tblReportDetail tbody').empty();
        //
        //   if(loadKeyWord == valCate){
        //     loadKeyWord = '';
        //     var sHtml = '<tr><td colspan="7" class="text-center">All</td></tr>';
        //   }else{
        //     loadKeyWord = valCate;
        //     var sHtml = '<tr><td colspan="7" class="text-center">'+loadKeyWord+'</td></tr>';
        //   }
        //   // console.log();
        //   // console.log(ev.event.view);
        //   $('#tblReportDetail tbody').append(sHtml);
        // });
      });
    </script>
</body>

</html>
<!-- end document-->
