<?php
$array = array("January","February","March","April","May","June","July","August","September","October","November","December");
foreach ($array as $key => $value) {
  $aFeedBack = unserialize($this->Report_model->getCntFeedback(($key+1),'All')['feedback']);
  $valFeedBack = 0;
  $valFeedBack += $aFeedBack['feedback_level1']+$aFeedBack['feedback_level2']+$aFeedBack['feedback_level3'];
  if(($this->Report_model->getCntFeedback(($key+1),'All')['cnt']*(3*3)) < 1){
    $valMax = 1;
  }else{
    $valMax = ($this->Report_model->getCntFeedback(($key+1),'All')['cnt']*(3*3));
  }
  $arrayName[$key] = array(
    'month_name' => $value,
    'cntAll' => ($valFeedBack/$valMax)*100,
  );
}
// echo "<pre>";
// print_r($arrayName);
// echo "<hr>";
// print_r($arrayName);
// echo "<hr>";
// echo json_encode($arrayName);
// die();
 ?>

<?php
$aUserAdmin = $this->session->userdata('userdata_admin');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>SysAdmin</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/font-face.css" rel="stylesheet" media="all">
    <!-- <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all"> -->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/theme.css" rel="stylesheet" media="all">

    <link href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
    <!-- <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all"> -->
    <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all">
    <style media="screen">
        body {
          font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        #chartdiv {
          width: 100%;
          min-height: 75vh;
        }

    </style>
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
</head>

<body class="animsition">
  <input type="hidden" id="base_url" value="<?php echo base_url("") ?>">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="#">
                            <strong class="text-danger" style="font-size:30px">HONDA LOCK</strong>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">

                            <li>
                                <a href="<?php echo base_url("helpdesk/admin_issue/list") ?>">
                                    <i class="fas fa-list-ol"></i>
                                    <span class="bot-line"></span>Issue List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url("helpdesk/admin_issue/report?year=".date('Y')."") ?>">
                                    <i class="fas fa-chart-bar"></i>
                                    <span class="bot-line"></span>Report</a>
                            </li>
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-cogs"></i>
                                    <span class="bot-line"></span>Option</a>
                                <ul class="header3-sub-list list-unstyled">
                                  <li>
                                    <a href="<?php echo base_url("helpdesk/admin_issue/member"); ?>"><i class="fas fa-users"></i> Member</a>
                                  </li>
                                  <!-- <li>
                                    <a href="button.html"><i class="fas fa-cogs"></i> Setting E-mail</a>
                                  </li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="header-button-item">
                          <a href="<?php echo base_url("helpdesk/admin_logout") ?>">
                            <i class="fas fa-sign-out-alt"></i> <span style="font-size:14px;padding-left: 5px;"> Logout</span>
                          </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">

            <!-- STATISTIC CHART-->
            <section class="statistic-chart">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 form-horizontal">
                          <br>

                          <div class="row form-group">
                              <div class="col-md-6 text-right">
                                  <strong class="title-5 m-b-35 text-center">Report issues :</strong>
                              </div>
                              <div class="col-md-2 text-left">
                                  <select name="select" id="select" class="form-control">
                                    <?php
                                    $arr = $this->Report_model->getYearIssue();
                                    if(!empty($arr)){
                                      foreach ($arr as $kArr => $value) {
                                        echo "<option value=".$value["years"].">".$value["years"]."</option>";
                                      }
                                    }else{
                                      echo "<option value=".date('Y').">".date('Y')."</option>";
                                    }
                                    ?>
                                  </select>
                              </div>
                              <div class="col-md-4">
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                          <div id="chartdiv"></div>
                          <center>
                            <table class="table table-bordered table-hover table-striped" style="width:500px">
                              <thead class="thead-dark">
                                <tr>
                                  <th scope="col" class="text-center">Level</th>
                                  <th scope="col" class="text-center">Point</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td class="text-center">ดีมาก</td>
                                  <td class="text-center">100-91</td>
                                </tr>
                                <tr>
                                  <td class="text-center">ดี</td>
                                  <td class="text-center">90-81</td>
                                </tr>
                                <tr>
                                  <td class="text-center">พอใช้</td>
                                  <td class="text-center">80-71</td>
                                </tr>
                                <tr>
                                  <td class="text-center">ควรปรับปรุง</td>
                                  <td class="text-center">70-61</td>
                                </tr>
                              </tbody>
                            </table>
                          </center>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC CHART-->

            <!-- COPYRIGHT-->
            <section class="">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.js"></script>

    <!-- Main JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>js/main.js"></script>
    <script src="<?php echo base_url("helpdesk/assets/lib/bootstrap-chosen-master/chosen-jquery.js")?>"></script>
    <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        var base_url = $('#base_url').val();

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart3D);
var aMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"]
chart.data = <?php echo json_encode($arrayName); ?>;

// Create axes
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "month_name";
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.renderer.minGridDistance = 30;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.title.text = "Issue Qty";
valueAxis.min = 0;
valueAxis.max = 100;
valueAxis.renderer.labels.template.adapter.add("text", function(text) {
  return text + "%";
});

// Create series
var series = chart.series.push(new am4charts.ColumnSeries3D());
series.dataFields.valueY = "cntAll";
series.dataFields.categoryX = "month_name";
series.name = "Year 2017";
series.clustered = false;
series.columns.template.tooltipText = "Issue in {month_name} (<?php echo date('Y') ?>): [bold]{valueY}[/]";
series.columns.template.fillOpacity = 0.9;
series.columns.template.url = "<?php echo base_url("/helpdesk/admin_issue/report/detail?year=".date('Y')."&month={month_name}"); ?>";
// series.columns.template.cursorTooltipEnabled  = false;

series.columns.template.adapter.add("fill", (fill, target) => {
  return chart.colors.getIndex(target.dataItem.index);
})

series.columns.template.adapter.add("stroke", (stroke, target) => {
  return chart.colors.getIndex(target.dataItem.index);
})

          // $.ajax({
          //   url: base_url+'helpdesk/feedback/getIssueByID',
          //   type: 'POST',
          //   dataType: 'json',
          //   data: {issue_id: recipient}
          // })
          // .done(function(res) {
          // })

      });
    </script>
</body>

</html>
<!-- end document-->
