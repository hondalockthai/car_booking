<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Login</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                            <h2>Help desk</h2>
                            <p><strong>Admin</strong></p>
                        </div>
                        <div class="login-form">
                            <form action="<?php echo base_url("helpdesk/admin/authen"); ?>" method="post">
                                <div class="form-group">
                                  <center>
                                    <?php
                                        if ($this->session->flashdata('msg-success')) {
                                    ?>
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">x</button>
                                        <?php echo $this->session->flashdata('msg-success'); ?>
                                    </div>
                                    <?php
                                    } // end if msg
                                    if ($this->session->flashdata('msg-danger')) {
                                    ?>
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">x</button>
                                        <?php echo $this->session->flashdata('msg-danger'); ?>
                                    </div>
                                    <?php
                                    } // end if msg
                                    if ($this->session->flashdata('msg-update')) {
                                    ?>
                                    <div class="alert alert-info alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert">x</button>
                                        <?php echo $this->session->flashdata('msg-update'); ?>
                                    </div>
                                    <?php
                                    }// end if msg
                                    ?>
                                  </center>
                                </div>
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="au-input au-input--full" type="text" name="sUsername" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="au-input au-input--full" type="password" name="sPassword" placeholder="Password">
                                </div>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>js/main.js"></script>

</body>

</html>
<!-- end document-->
