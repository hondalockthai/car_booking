<?php
$aUserAdmin = $this->session->userdata('userdata_admin');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>SysAdmin</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/font-face.css" rel="stylesheet" media="all">
    <!-- <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all"> -->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/theme.css" rel="stylesheet" media="all">

    <link href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
    <!-- <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all"> -->
    <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all">

</head>

<body class="animsition">
  <input type="hidden" id="base_url" value="<?php echo base_url("") ?>">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="#">
                            <strong class="text-danger" style="font-size:30px">Honda Lock</strong>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">

                            <li>
                                <a href="<?php echo base_url("helpdesk/admin_issue/list") ?>">
                                    <i class="fas fa-list-ol"></i>
                                    <span class="bot-line"></span>Issue List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url("helpdesk/admin_issue/report?year=".date('Y')."") ?>">
                                    <i class="fas fa-chart-bar"></i>
                                    <span class="bot-line"></span>Report</a>
                            </li>
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-cogs"></i>
                                    <span class="bot-line"></span>Option</a>
                                <ul class="header3-sub-list list-unstyled">
                                  <li>
                                    <a href="<?php echo base_url("helpdesk/admin_issue/member"); ?>"><i class="fas fa-users"></i> Member</a>
                                  </li>
                                  <!-- <li>
                                    <a href="button.html"><i class="fas fa-cogs"></i> Setting E-mail</a>
                                  </li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="header-button-item">
                          <a href="<?php echo base_url("helpdesk/admin_logout") ?>">
                            <i class="fas fa-sign-out-alt"></i> <span style="font-size:14px;padding-left: 5px;"> Logout</span>
                          </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">

            <!-- STATISTIC CHART-->
            <section class="statistic-chart">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Issue List</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                          <table id="example" class="display" style="width:100%">
                            <thead>
                                <tr>
                                  <th>Create Date</th>
                                  <th>Issue No</th>
                                  <th>Type of Request</th>
                                  <th>Computer Name</th>
                                  <th>Status</th>
                                  <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($aData as $kData => $vData) {?>
                                      <tr>
                                          <td><?php echo $vData["createdate"]; ?></td>
                                          <td><?php echo sprintf("%05d", $vData["issue_id"]); ?></td>

                                          <td><?php $aType = unserialize($aData[0]["type_of_req"]) ?>
                                            <?php foreach ($aType as $kType => $vType) {
                                                    echo $vType;
                                                    if($kType < (count($aType)-1)){
                                                      echo ",";
                                                    }
                                                  } ?>
                                          </td>
                                          <td><?php echo $vData["com_name"]; ?></td>
                                          <?php
                                              if($vData["status"] == 0){
                                                if(!empty($vData["approve_manager_by"])){
                                                  echo '<td class="text-center"><span class="badge badge-success">Approve</span></td>';
                                                }else{
                                                  echo '<td class="text-center"><span class="badge badge-secondary">Waiting</span></td>';
                                                }
                                                ?>
                                        <?php }else if($vData["status"] == 1){?>
                                                <td class="text-center"><span class="badge badge-success">Success</span></td>
                                        <?php }else if($vData["status"] == 2){?>
                                                <td class="text-center"><span class="badge badge-danger">Cancel</span></td>
                                        <?php }else if($vData["status"] == 3){?>
                                                <td class="text-center"><span class="badge badge-warning">In Progress</span></td>
                                        <?php }?>
                                          <td>
                                            <?php if($vData['status'] == 0){ ?>
                                            <div class="dropdown">
                                              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-cog" aria-hidden="true"></i>
                                              </button>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="<?php echo base_url("helpdesk/admin_issue/view?issue_id=".$vData["issue_id"]."") ?>"><i class="fa fa-eye"></i>&nbsp; View</a>
                                                <a class="dropdown-item" href="<?php echo base_url("helpdesk/admin_issue/repair?issue_id=".$vData["issue_id"]."") ?>"><i class="fas fa-edit"></i> Repair</a>
                                                <hr>
                                                <a class="dropdown-item text-success cancelBtn" href="<?php echo base_url("helpdesk/admin_issue/success?issue_id=".$vData["issue_id"]."") ?>"><i class="fas fa-clipboard-check"></i> Success</a>
                                                <a class="dropdown-item text-warning cancelBtn" href="<?php echo base_url("helpdesk/admin_issue/in_progress?issue_id=".$vData["issue_id"]."") ?>"><i class="fas fa-spinner"></i> In Progress</a>
                                                <a class="dropdown-item text-danger cancelBtn" href="<?php echo base_url("helpdesk/admin_issue/cancel?issue_id=".$vData["issue_id"]."") ?>"><i class="fas fa-trash"></i> Reject</a>
                                              </div>
                                            </div>
                                          <?php }else if($vData['status'] == 1){ //success ?>
                                            <div class="dropdown">
                                              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-cog" aria-hidden="true"></i>
                                              </button>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="<?php echo base_url("helpdesk/admin_issue/view?issue_id=".$vData["issue_id"]."") ?>"><i class="fa fa-eye"></i>&nbsp; View</a>
                                                <hr>
                                                <button type="button" class="dropdown-item text-info" data-toggle="modal" data-target="#largeModal" data-whatever="<?php echo $vData["issue_id"] ?>"><i class="fas fa-comments"></i> Feedback</button>
                                              </div>
                                            </div>
                                          <?php }else if($vData['status'] == 2){ //cencel ?>
                                            <div class="dropdown">
                                              <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-cog" aria-hidden="true"></i>
                                              </button>
                                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="<?php echo base_url("helpdesk/admin_issue/view?issue_id=".$vData["issue_id"]."") ?>"><i class="fa fa-eye"></i>&nbsp; View</a>
                                              </div>
                                            </div>
                                          <?php }else if($vData['status'] == 3){ //In Progress ?>
                                          <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i class="fa fa-cog" aria-hidden="true"></i>
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                              <a class="dropdown-item" href="<?php echo base_url("helpdesk/admin_issue/view?issue_id=".$vData["issue_id"]."") ?>"><i class="fa fa-eye"></i>&nbsp; View</a>
                                              <a class="dropdown-item" href="<?php echo base_url("helpdesk/admin_issue/repair?issue_id=".$vData["issue_id"]."") ?>"><i class="fas fa-edit"></i> Repair</a>
                                              <hr>
                                              <a class="dropdown-item text-success cancelBtn" href="<?php echo base_url("helpdesk/admin_issue/success?issue_id=".$vData["issue_id"]."") ?>"><i class="fas fa-clipboard-check"></i> Success</a>
                                              <a class="dropdown-item text-danger cancelBtn" href="<?php echo base_url("helpdesk/admin_issue/cancel?issue_id=".$vData["issue_id"]."") ?>"><i class="fas fa-trash"></i> Reject</a>
                                            </div>
                                          </div>
                                          <?php } ?>
                                            <!--  -->
                                          </td>
                                      </tr>
                              <?php } ?>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC CHART-->

            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>

    <!-- modal large -->
    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="largeModalLabel">Feedback</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?php echo base_url("helpdesk/feedback"); ?>" method="post">
              <input type="hidden" name="issue_id" id="issue_id" value="">
              <div class="row">
                  <div class="col-md-12">
                      <!-- DATA TABLE-->
                      <div class="table-responsive">
                          <table class="table table-borderless table-data3">
                              <thead>
                                  <tr>
                                      <th style="width:30px">No.</th>
                                      <th>Topic</th>
                                      <th class="text-center"><i class="far fa-smile" style="font-size:50px"></i><br>Excellent</th>
                                      <th class="text-center"><i class="far fa-meh" style="font-size:50px"></i><br>Normal</th>
                                      <th class="text-center"><i class="far fa-frown" style="font-size:50px"></i><br>Bad</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td style="width:30px">1</td>
                                      <td style="padding-left: 10px;">แก้ไขปัญหาได้ตรงตามการแจ้งซ่อม</td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level1" value="3" class="form-check-input feedback_level1" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level1" value="2" class="form-check-input feedback_level1" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level1" value="1" class="form-check-input feedback_level1" disabled></td>
                                  </tr>
                                  <tr>
                                      <td style="width:30px">2</td>
                                      <td style="padding-left: 10px;">ความรวดเร็วในการแก้ไขปัญหา (ไม่กระทบกับการทำงานของ User)</td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level2" value="3" class="form-check-input feedback_level2" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level2" value="2" class="form-check-input feedback_level2" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level2" value="1" class="form-check-input feedback_level2" disabled></td>
                                  </tr>
                                  <tr>
                                      <td style="width:30px">3</td>
                                      <td style="padding-left: 10px;">การแนะนำให้ความรู้เกี่ยวกับคอมพิวเตอร์ (ป้องกันการเกิดปัญหาซ้ำ)</td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level3" value="3" class="form-check-input feedback_level3" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level3" value="2" class="form-check-input feedback_level3" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level3" value="1" class="form-check-input feedback_level3" disabled></td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                      <!-- END DATA TABLE-->
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- end modal large -->

    <!-- Jquery JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.js"></script>

    <!-- Main JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>js/main.js"></script>
    <script src="<?php echo base_url("helpdesk/assets/lib/bootstrap-chosen-master/chosen-jquery.js")?>"></script>
    <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        var base_url = $('#base_url').val();
        $('#example').DataTable({
            "order": [[ 0, "desc" ]]
        });

        $('select').css('border', '1px solid gainsboro');
        $('select').css('box-shadow', '0 0 1px rgb(144, 144, 144)');
        $('input').css('border', '1px solid gainsboro');
        $('input').css('box-shadow', '0 0 1px rgb(144, 144, 144)');

        $('body').on('click','.cancelBtn',function(){

          var lnk = $(this).attr('href');
          console.log(lnk);
          if(confirm('Please Confirm')){
            window.location.href  = lnk;
          }
          return false;
        });

        $('#largeModal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var recipient = button.data('whatever') // Extract info from data-* attributes
          // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
          // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
          var modal = $(this)
          $.ajax({
            url: base_url+'helpdesk/feedback/getIssueByID',
            type: 'POST',
            dataType: 'json',
            data: {issue_id: recipient}
          })
          .done(function(res) {
            $.each(modal.find('.modal-body input.feedback_level1') , function(index, el) {
              if($(this).val() == res.feedback_level1){
                $(this).prop('checked',true);
              }
            });
            $.each(modal.find('.modal-body input.feedback_level2') , function(index, el) {
              if($(this).val() == res.feedback_level2){
                $(this).prop('checked',true);
              }
            });
            $.each(modal.find('.modal-body input.feedback_level3') , function(index, el) {
              if($(this).val() == res.feedback_level3){
                $(this).prop('checked',true);
              }
            });
          })

        })
      });
    </script>
</body>

</html>
<!-- end document-->
