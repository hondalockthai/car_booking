<?php
$aUserAdmin = $this->session->userdata('userdata_admin');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>SysAdmin</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/theme.css" rel="stylesheet" media="all">

    <link href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all">

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

</head>

<body class="animsition">
  <input type="hidden" id="base_url" value="<?php echo base_url("") ?>">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="#">
                            <strong class="text-danger" style="font-size:30px">HONDA LOCK</strong>
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">

                            <li>
                                <a href="<?php echo base_url("helpdesk/admin_issue/list") ?>">
                                    <i class="fas fa-list-ol"></i>
                                    <span class="bot-line"></span>Issue List</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url("helpdesk/admin_issue/report?year=".date('Y')."") ?>">
                                    <i class="fas fa-chart-bar"></i>
                                    <span class="bot-line"></span>Report</a>
                            </li>
                            <li class="has-sub">
                                <a href="#">
                                    <i class="fas fa-cogs"></i>
                                    <span class="bot-line"></span>Option</a>
                                <ul class="header3-sub-list list-unstyled">
                                  <li>
                                    <a href="<?php echo base_url("helpdesk/admin_issue/member"); ?>"><i class="fas fa-users"></i> Member</a>
                                  </li>
                                  <!-- <li>
                                    <a href="button.html"><i class="fas fa-cogs"></i> Setting E-mail</a>
                                  </li> -->
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="header-button-item">
                          <a href="<?php echo base_url("helpdesk/admin_logout") ?>">
                            <i class="fas fa-sign-out-alt"></i> <span style="font-size:14px;padding-left: 5px;"> Logout</span>
                          </a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">

            <!-- STATISTIC CHART-->
            <section class="statistic-chart" style="min-height: 120vh;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35">Member List</h3>
                            <!-- <a href="<?php echo base_url("helpdesk/admin_issue/member/form") ?>" class="btn btn-primary">
                                            <i class="fas fa-plus"></i>&nbsp; Add Member</a> -->
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                          <table id="example" class="display" style="width:100%">
                            <thead>
                                <tr>
                                  <th>No.</th>
                                  <th>Username</th>
                                  <th>Firstname</th>
                                  <th>Lastname</th>
                                  <th>Department Name</th>
                                  <th>Tel.</th>
                                  <th>Status</th>
                                  <!-- <th>Action</th> -->
                                </tr>
                            </thead>
                            <tbody>
                              <?php foreach ($aData as $kData => $vData) {?>
                                      <tr>
                                          <td><?php echo ($kData+1); ?></td>
                                          <td><?php echo $vData["username"]; ?></td>
                                          <td><?php echo $vData["firstname"]; ?></td>
                                          <td><?php echo $vData["lastname"]; ?></td>
                                          <td><?php echo $vData["department_name"]; ?></td>
                                          <td><?php echo $vData["tel"]; ?></td>

                                          <?php if($vData["is_manager"] == 1){ ?>
                                            <td><span class="badge badge-primary">Manager</span></td>
                                          <?php }else{?>
                                            <td><span class="badge badge-light">Member</span></td>
                                          <?php } ?>
                                          <!-- <td> -->
                                            <!-- <button type="button" class="btn btn-outline-primary"><i class="fa fa-star"></i>&nbsp;</button> -->
                                            <!-- <?php if($vData["is_manager"] == 1){ ?>
                                            <button data-href="<?php echo base_url("helpdesk/admin_issue/updateManager"); ?>" data-stat="0" data-user_id="<?php echo $vData["user_id"] ?>" class="btn btn-outline-primary" id="alertManagerDisabled" disabled><i class="fa fa-star"></i></button>
                                          <?php }else{?>
                                            <button data-href="<?php echo base_url("helpdesk/admin_issue/updateManager"); ?>" data-stat="1" data-user_id="<?php echo $vData["user_id"] ?>" class="btn btn-outline-primary" id="alertManagerEnabled"><i class="fa fa-star"></i></button>
                                          <?php } ?> -->
                                            <!-- <a href="<?php echo base_url("helpdesk/admin_issue/member/form?user_id=".$vData["user_id"].""); ?>" class="btn btn-outline-primary"><i class="fa fa-eye"></i></a> -->
                                            <!-- <button type="button" class="btn btn-danger"><i class="fa fa-trash-alt"></i></button> -->
                                          <!-- </td> -->
                                      </tr>
                              <?php } ?>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC CHART-->

            <!-- COPYRIGHT-->
            <section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>Copyright © 2018 Colorlib. All rights reserved. Template by <a href="https://colorlib.com">Colorlib</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>

    <!-- modal large -->
    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h3 class="modal-title" id="largeModalLabel">Feedback</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form action="<?php echo base_url("helpdesk/feedback"); ?>" method="post">
              <input type="hidden" name="issue_id" id="issue_id" value="">
              <div class="row">
                  <div class="col-md-12">
                      <!-- DATA TABLE-->
                      <div class="table-responsive">
                          <table class="table table-borderless table-data3">
                              <thead>
                                  <tr>
                                      <th style="width:30px">No.</th>
                                      <th>Topic</th>
                                      <th class="text-center"><i class="far fa-smile" style="font-size:50px"></i><br>Excellent</th>
                                      <th class="text-center"><i class="far fa-meh" style="font-size:50px"></i><br>Normal</th>
                                      <th class="text-center"><i class="far fa-frown" style="font-size:50px"></i><br>Bad</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <tr>
                                      <td style="width:30px">1</td>
                                      <td style="padding-left: 10px;">แก้ไขปัญหาได้ตรงตามการแจ้งซ่อม</td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level1" value="3" class="form-check-input feedback_level1" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level1" value="2" class="form-check-input feedback_level1" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level1" value="1" class="form-check-input feedback_level1" disabled></td>
                                  </tr>
                                  <tr>
                                      <td style="width:30px">2</td>
                                      <td style="padding-left: 10px;">ความรวดเร็วในการแก้ไขปัญหา (ไม่กระทบกับการทำงานของ User)</td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level2" value="3" class="form-check-input feedback_level2" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level2" value="2" class="form-check-input feedback_level2" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level2" value="1" class="form-check-input feedback_level2" disabled></td>
                                  </tr>
                                  <tr>
                                      <td style="width:30px">3</td>
                                      <td style="padding-left: 10px;">การแนะนำให้ความรู้เกี่ยวกับคอมพิวเตอร์ (ป้องกันการเกิดปัญหาซ้ำ)</td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level3" value="3" class="form-check-input feedback_level3" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level3" value="2" class="form-check-input feedback_level3" disabled></td>
                                      <td class="text-center" style="padding-left: 65px;"><input type="radio" required name="feedback_level3" value="1" class="form-check-input feedback_level3" disabled></td>
                                  </tr>
                              </tbody>
                          </table>
                      </div>
                      <!-- END DATA TABLE-->
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    <!-- end modal large -->

    <!-- Jquery JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.js"></script>

    <!-- Main JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>js/main.js"></script>
    <script src="<?php echo base_url("helpdesk/assets/lib/bootstrap-chosen-master/chosen-jquery.js")?>"></script>
    <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        var base_url = $('#base_url').val();
        $('#example').DataTable();
        $('select').css('border', '1px solid gainsboro');
        $('select').css('box-shadow', '0 0 1px rgb(144, 144, 144)');
        $('input').css('border', '1px solid gainsboro');
        $('input').css('box-shadow', '0 0 1px rgb(144, 144, 144)');

        $('body').on('click','.cancelBtn',function(){

          var lnk = $(this).attr('href');
          console.log(lnk);
          if(confirm('Please Confirm')){
            window.location.href  = lnk;
          }
          return false;
        });

        $('#largeModal').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var recipient = button.data('whatever') // Extract info from data-* attributes
          // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
          // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
          var modal = $(this)
          $.ajax({
            url: base_url+'helpdesk/feedback/getIssueByID',
            type: 'POST',
            dataType: 'json',
            data: {issue_id: recipient}
          })
          .done(function(res) {
            $.each(modal.find('.modal-body input.feedback_level1') , function(index, el) {
              if($(this).val() == res.feedback_level1){
                $(this).prop('checked',true);
              }
            });
            $.each(modal.find('.modal-body input.feedback_level2') , function(index, el) {
              if($(this).val() == res.feedback_level2){
                $(this).prop('checked',true);
              }
            });
            $.each(modal.find('.modal-body input.feedback_level3') , function(index, el) {
              if($(this).val() == res.feedback_level3){
                $(this).prop('checked',true);
              }
            });
          })

        })

        $('body').on('click','#alertManager',function(){
          console.log($(this).data("href"));
          console.log($(this).data("user_id"));
          console.log($(this).data("stat"));
          swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this imaginary file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              swal("Poof! Your imaginary file has been deleted!", {
                icon: "success",
              });
            } else {
              swal("Your imaginary file is safe!");
            }
          });
        });
      });
    </script>
</body>

</html>
<!-- end document-->
