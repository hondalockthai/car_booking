<?php
$userdata_admin = $this->session->userdata('userdata_admin');
// echo "<pre>";
// print_r($userdata_admin);
// die();
$aSerial = unserialize($aData['type_of_req']);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Forms</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>css/theme.css" rel="stylesheet" media="all">

    <link href="<?php echo base_url()."helpdesk/assets/lib/bootstrap-chosen-master/bootstrap-chosen.css" ?>" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- PAGE CONTAINER-->
        <div class="page-register" style="min-height: 100vh;">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-10"  style="top: 70px !important; min-height: 84vh;padding-bottom: 100px;">
                              <div class="card">
                                    <div class="card-header text-center">
                                        <strong class="text-danger pull-left">Honda Lock</strong>
                                        <strong>แบบบันทึกการแจ้งซ่อม Hardware, Software และ Network</strong>
                                        <?php if($this->input->get('issue_no') != ''){ ?>
                                                <span class="pull-right"><strong>Issue No : </strong><u>00001</u></span>
                                        <?php }else{ ?>
                                                <span class="pull-right" style="display:none;"><strong>Issue No : </strong><u>00001</u></span>
                                        <?php } ?>
                                    </div>
                                    <div class="card-body card-block">
                                        <form action="<?php echo base_url("helpdesk/admin_issue/update"); ?>" method="post" class="form-horizontal">
                                            <div class="row form-group">
                                              <div class="col col-md-12">
                                                <center>

                                                  <?php
                                                      if ($this->session->flashdata('msg-success')) {
                                                  ?>
                                                  <div class="alert alert-success alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-success'); ?>
                                                  </div>
                                                  <?php
                                                  } // end if msg
                                                  if ($this->session->flashdata('msg-danger')) {
                                                  ?>
                                                  <div class="alert alert-danger alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-danger'); ?>
                                                  </div>
                                                  <?php
                                                  } // end if msg
                                                  if ($this->session->flashdata('msg-update')) {
                                                  ?>
                                                  <div class="alert alert-info alert-dismissible" role="alert">
                                                      <button type="button" class="close" data-dismiss="alert">x</button>
                                                      <?php echo $this->session->flashdata('msg-update'); ?>
                                                  </div>
                                                  <?php
                                                  }// end if msg
                                                  ?>

                                                </center>
                                              </div>
                                            </div>


                                            <div class="row form-group">
                                              <div class="col col-md-12">
                                                <center><strong><u>ส่วนของ ผู้ทำการแจ้งซ่อม</u></strong></center>
                                                <input type="hidden" name="issue_id" value="<?php echo $this->input->get('issue_id'); ?>">
                                              </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sFullName" class=" form-control-label">Name of Requster :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="sFullName" class="form-control" maxlength="50" required disabled value="<?php echo $aData["name_of_req"]; ?>">
                                                </div>
                                            </div>

                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sCompName" class=" form-control-label">Computer name :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="sCompName" class="form-control" maxlength="50" required disabled value="<?php echo $aData["com_name"]; ?>">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="iDept" class=" form-control-label">Department :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="iDept" class="form-control"  maxlength="30" required disabled value="<?php echo $aData['dept_name']; ?>">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="sTel" class=" form-control-label">Phone No. :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="sTel" class="form-control"  maxlength="30" required disabled value="<?php echo $aData["phone_no"] ?>">
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label">Type of request :</label>
                                                </div>

                                                <div class="col col-md-7">
                                                    <div class="form-check-inline form-check">
                                                        <label for="inline-checkbox1" class="form-check-label " style="padding-right: 30px;">
                                                            <input type="checkbox" <?php echo (in_array("Hardware",$aSerial))?'checked':''; ?> id="inline-checkbox1" name="inline-checkbox[]" value="Hardware" disabled class="form-check-input" >Hardware
                                                        </label>
                                                        <label for="inline-checkbox2" class="form-check-label " style="padding-right: 30px;">
                                                            <input type="checkbox" <?php echo (in_array("Software",$aSerial))?'checked':''; ?> id="inline-checkbox2" name="inline-checkbox[]" value="Software" disabled class="form-check-input">Software
                                                        </label>
                                                        <label for="inline-checkbox3" class="form-check-label " style="padding-right: 30px;">
                                                            <input type="checkbox" <?php echo (in_array("Network",$aSerial))?'checked':''; ?> id="inline-checkbox3" name="inline-checkbox[]" value="Network" disabled class="form-check-input">Network
                                                        </label>
                                                        <!-- <label for="inline-checkbox4" class="form-check-label ">
                                                            <input type="checkbox" id="inline-checkbox4" name="inline-checkbox4" value="option3" class="form-check-input">Other
                                                        </label> -->
                                                    </div>
                                                </div>
                                                <!-- <div class="col col-md-2">
                                                    <input type="text" name="sCompName" class="form-control"  maxlength="30" required <?php echo $aUserdata["tel"] ?>>
                                                </div> -->
                                            </div>
                                            <hr>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="iDept" class=" form-control-label">Case of Problem :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <textarea class="form-control" name="sDetail" rows="8" cols="80" required disabled><?php echo $aData["case_of_problem"] ?></textarea>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row form-group">
                                              <div class="col col-md-12">
                                                <center><strong><u>ส่วนของ ผู้ดูแลระบบคอมพิวเตอร์</u></strong></center>
                                              </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="case_of_problem_it" class=" form-control-label">Case of Problem :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <textarea class="form-control" name="case_of_problem_it" rows="8" cols="80" required ><?php echo $aData["case_of_problem_it"] ?></textarea>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="corrective_action_detail" class=" form-control-label">Corrective Action :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <textarea class="form-control" name="corrective_action_detail" rows="8" cols="80" required ><?php echo $aData["corrective_action_detail"] ?></textarea>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label class=" form-control-label"></label>
                                                </div>

                                                <div class="col col-md-7">
                                                    <div class="form-check-inline form-check">
                                                        <label for="inline-radio1" class="form-check-label " style="padding-right: 30px;">
                                                            <input type="radio" id="inline-radio1" name="corrective_action" value="1" class="form-check-input radioChk" checked required>Repair By IT
                                                        </label>
                                                        <label for="inline-radio2" class="form-check-label " style="padding-right: 30px;">
                                                            <input type="radio" id="inline-radio2" name="corrective_action" value="2" class="form-check-input radioChk" required>Sent to out side
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group" id="forIT">
                                                <div class="col col-md-3">
                                                    <label for="corrective_action_remark" class=" form-control-label">Solution :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <textarea class="form-control" name="corrective_action_remark" rows="8" cols="80" required ><?php echo $aData["corrective_action_remark"] ?></textarea>
                                                </div>
                                            </div>
                                            <div class="row form-group" id="forSupplier" style="display:none;">
                                                <div class="col col-md-3">
                                                    <label for="corrective_action_remark" class=" form-control-label">Name of Supplier :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <input type="text" name="corrective_action_remark" class="form-control" required value="<?php echo $aData["corrective_action_remark"] ?>">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="finished_date" class=" form-control-label">Finished Date :</label>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <input type="date" name="finished_date" class="form-control" required value="<?php echo $aData["finished_date"] ?>">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="received_date" class=" form-control-label">Received Date :</label>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <input type="date" name="received_date" class="form-control" disabled value="<?php echo $aData["received_date"] ?>">
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="repair_by" class=" form-control-label">Repair By :</label>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <input type="text" name="repair_by" class="form-control" readonly value="<?php echo $aData["repair_by"] ?>">
                                                </div>
                                            </div>

                                            <hr>
                                            <div class="row form-group">
                                                <div class="col col-md-3">
                                                    <label for="preventive_action" class=" form-control-label">Preventive Action :</label>
                                                </div>
                                                <div class="col-12 col-md-9">
                                                    <textarea class="form-control" name="preventive_action" rows="8" cols="80" required ><?php echo $aData["preventive_action"] ?></textarea>
                                                </div>
                                            </div>
                                          </div>
                                      <div class="card-footer">
                                        <center>
                                          <a href="<?php echo base_url("helpdesk/admin_issue/list") ?>" class="btn btn-danger btn-sm">
                                            <i class="fa fa-chevron-left"></i> Back
                                          </a>
                                        </center>
                                      </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </div>

    <!-- Jquery JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/wow/wow.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>vendor/select2/select2.min.js">
    </script>

    <!-- Main JS-->
    <script src="<?php echo base_url()."helpdesk/assets/template/CoolAdmin-master/"?>js/main.js"></script>
    <script src="<?php echo base_url("helpdesk/assets/lib/bootstrap-chosen-master/chosen-jquery.js")?>"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        $('.select-chosen').chosen();
        $('input').prop('disabled',true);
        $('textarea').prop('disabled',true);
      });
    </script>
</body>

</html>
<!-- end document-->
